import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;


public class PersistenceServer {
    private static final int CMD_STATE_ANSWER = 0;
    private static final int CMD_START = 1;
    private static final int CMD_RESET = 2;

    
	public static final int REQUEST_STATE_PORT = 10;
	public static final boolean PHASE_TEST = false;
	static boolean MAIN_SERVER = true; // IF false, only update data base to duplicate data but doesn't answer.

	int port;
	String url;
	String fileDataBase="./DataBaseXP.data";



	public PersistenceServer(int port, String url) { // TODO fileDataBase as input
		super();
		if (PHASE_TEST) {
			try {Files.deleteIfExists(Paths.get(this.fileDataBase));} catch (IOException e) {e.printStackTrace();}
		}
		this.port = port;
		this.url = url;
	}


	public static void main(String[] args) {
		try {
			System.out.println("Start Persistence Server");
			PersistenceServer PS = new PersistenceServer(10000, "/ServerPersistence");
			HttpServer server;
			server = HttpServer.create(new InetSocketAddress(PS.port), 0);
			server.createContext(PS.url, new httpHandler(PS));
			server.setExecutor(null); // creates a default executor
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	static class httpHandler implements HttpHandler {
		private static final int NODE_RED_PORT = 1990;
		PersistenceServer PS;
		public httpHandler(PersistenceServer pS) {
			this.PS = pS;
		}

		@Override
		public void handle(HttpExchange httpExchange) throws IOException {
			String response = "Response";
			String httpPost = getStringFromInputStream(httpExchange.getRequestBody());

			System.out.print("\nReceived Data: ");
			parseReceivedJSON(httpPost);

			httpExchange.sendResponseHeaders(200, response.length());
			OutputStream os = httpExchange.getResponseBody();
			os.write(response.getBytes());
			os.close();

		}

		private byte[] parseReceivedJSON(String httpPost) {
			try {
				JSONParser parser = new JSONParser();
				Object obj;
				obj = parser.parse(httpPost);
				JSONObject jsonObject = (JSONObject)obj;

				if (!jsonObject.containsKey("hardware_serial")
						|| !jsonObject.containsKey("port")
						|| !jsonObject.containsKey("counter")
						|| !jsonObject.containsKey("payload_raw")) {
					return null;
				}

				String hardware_serial = jsonObject.get("hardware_serial").toString();
				int port = Integer.parseInt(jsonObject.get("port").toString());
				if (port == REQUEST_STATE_PORT ) {
					System.out.print("CMD[Request State].");
					if (MAIN_SERVER) {
						String stateMsg = getLineState(hardware_serial);
						PostCommand(stateMsg,hardware_serial);
					}
				}else {
					System.out.print("DATA[Update State].");
					updateLineState(hardware_serial,jsonObject);
				} 
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}



		private void PostCommand(String downlinkData, String deviceNodeRedUrl) throws InterruptedException {
			URL url;
			try {
				url = new URL("http://localhost:"+NODE_RED_PORT+"/"+deviceNodeRedUrl);

				URLConnection con = url.openConnection();
				HttpURLConnection http = (HttpURLConnection)con;
				http.setRequestMethod("POST"); // PUT is another valid option
				http.setDoOutput(true);
				byte[] out = downlinkData.getBytes();
				int length = out.length;
				http.setFixedLengthStreamingMode(length);
				http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				http.connect();

				try(OutputStream os = http.getOutputStream()) {
					os.write(out);
				}
				http.disconnect();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("Failed to trigger node-red ["+"http://localhost:"+NODE_RED_PORT+"/"+deviceNodeRedUrl+"].\n Retry in 1s.");//e.printStackTrace();
				Thread.sleep(1000);
				PostCommand(downlinkData,deviceNodeRedUrl);
			}
		}			


		private int findLineHardwareSerial(List<String> dataLines, String hardwareSerial) {
			for (int i=0;i<dataLines.size();i++) {
				String[] line = dataLines.get(i).split("-");
				if (line[1].equals(hardwareSerial)) {
					return i;
				}
			}
			return -1;
		}

		private String updateLineState(String hardwareSerial, JSONObject jsonObject) {
			List<String> dataLines;
			Paths.get(this.PS.fileDataBase);
			
			try {
				Files.write(Paths.get(this.PS.fileDataBase), "".getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
				dataLines = Files.readAllLines(Paths.get(this.PS.fileDataBase));
				int numLine = findLineHardwareSerial(dataLines, hardwareSerial);
				if (numLine!=-1) {
					dataLines.remove(numLine);
				}
				String payloadRawJson = jsonObject.get("payload_raw").toString();
				JSONObject payloadRawJsonObject = (JSONObject)(new JSONParser()).parse(payloadRawJson);
				JSONArray jsonDataArray = (JSONArray) payloadRawJsonObject.get("data");

				int configurationBase = Integer.parseInt((jsonDataArray.get(0)).toString());
				int offset            = Integer.parseInt((jsonDataArray.get(1)).toString());
				int lorawanFrameCounter = Integer.parseInt(jsonObject.get("counter").toString());
				int xpState = Integer.parseInt(jsonObject.get("port").toString());

				String dataBaseEntry = "-"+hardwareSerial +"-"+configurationBase	+"-"+offset +"-"+xpState +"-"+lorawanFrameCounter;
				System.out.println(" -> WRITE[" + dataBaseEntry +"]" );

				dataLines.add(dataBaseEntry);

				//Rewritre the database file
				Files.delete(Paths.get(this.PS.fileDataBase));
				for (int i=0; i<dataLines.size(); i++) {
					Files.write(Paths.get(this.PS.fileDataBase), (dataLines.get(i)+"\n").getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
				}
			} catch (IOException e) { e.printStackTrace();
			} catch (ParseException e) { e.printStackTrace();}


			return null;
		}

		private String getLineState(String hardwareSerial) {
			List<String> dataLines = null;
			Paths.get(this.PS.fileDataBase);
			String retval = null; 
			int numLine;
			int command, configBase, offset, xpState, lorawanFrameCnt;
			String dataBaseEntry;
			try {
				dataLines = Files.readAllLines(Paths.get(this.PS.fileDataBase));
				numLine = findLineHardwareSerial(dataLines, hardwareSerial);
			} catch	(IOException e) { numLine=-1; }// File doesn't exist, will be created

			if (numLine!=-1) {
				command = CMD_STATE_ANSWER;
				String[] line = dataLines.get(numLine).split("-");
				configBase = ((Integer.parseInt(line[2]))& 0xFF);
				offset = ((Integer.parseInt(line[3]))& 0xFF);
				xpState = ((Integer.parseInt(line[4]))& 0xFF);
				lorawanFrameCnt = ((Integer.parseInt(line[5]))& 0xFF);
				dataBaseEntry = "-" + hardwareSerial + "-" + configBase + "-" + offset + "-" + xpState + "-" + lorawanFrameCnt; 
			}else {
			        command = CMD_START;
				configBase =  0; // TODO delete car pas lu par le device, le commmande suffit.
				offset = 0;
				xpState = 1;
				lorawanFrameCnt = 1;
				dataBaseEntry = "*new*-" + hardwareSerial + "-" + configBase + "-" + offset + "-" + xpState + "-" + lorawanFrameCnt; 
				//				try {Files.write(Paths.get(this.PS.fileDataBase), (dataBaseEntry+"\n").getBytes(), StandardOpenOption.APPEND, StandardOpenOption.CREATE);}
				// catch (IOException e) { e.printStackTrace(); }

			}
			System.out.println(" -> RESPONSE[" + dataBaseEntry +"]" );

			retval = "msg=" 
					+ String.format("%02X",((byte)command)) 
					+ String.format("%02X",((byte)configBase)) 
					+ String.format("%02X",((byte)offset)) 
					+ String.format("%02X",((byte)xpState)) 
					+ String.format("%08X",(lorawanFrameCnt)); 

			return retval;
		}


		// convert InputStream to String
		public static String getStringFromInputStream(InputStream is) {
			BufferedReader br = null;
			StringBuilder sb = new StringBuilder();
			String line;

			try {
				br = new BufferedReader(new InputStreamReader(is));
				while ((line = br.readLine()) != null) {
					sb.append(line);
					sb.append('\n');
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			return sb.toString();
		}


	}
}
