#include "LoRaMacHandlers.h"


/* [IN] nbTrans in [1;15] */
void setCodingRate(uint8_t cr){
    MibRequestConfirm_t mibReq;
    mibReq.Type = MIB_CHANNELS_CODING_RATE;
    mibReq.Param.ChannelsCodingRate=cr;
    LoRaMacMibSetRequestConfirm( &mibReq );
}


/* [IN] nbTrans in [1;15] */
void setNbRep(uint8_t nbTrans){
    MibRequestConfirm_t mibReq;
    mibReq.Type = MIB_CHANNELS_NB_TRANS;
    mibReq.Param.ChannelsNbTrans=nbTrans;
    LoRaMacMibSetRequestConfirm( &mibReq );
}

/* [IN] power in 
 * TX_POWER_0=14dBm
 * TX_POWER_1=12dBm
 * TX_POWER_2=10dBm
 * TX_POWER_3=8dBm
 * TX_POWER_4=6dBm
 * TX_POWER_5=4dBm
 * TX_POWER_6=2dBm
 * TX_POWER_7=0dBm */
void setTxPower(uint8_t power){
    MibRequestConfirm_t mibReq;
    mibReq.Type = MIB_CHANNELS_TX_POWER;
    mibReq.Param.ChannelsTxPower = power;
    LoRaMacMibSetRequestConfirm( &mibReq );
}

/* [IN] dataRate in
 * DR_0 SF12 - BW125
 * DR_1 SF11 - BW125
 * DR_2 SF10 - BW125
 * DR_3 SF9 - BW125
 * DR_4 SF8 - BW125
 * DR_5 SF7 - BW125
 * DR_6 SF7 - BW250 */
void setDataRate(uint8_t dataRate){
    MibRequestConfirm_t mibReq;
    mibReq.Type = MIB_CHANNELS_DATARATE;
    mibReq.Param.ChannelsDatarate = dataRate;
    LoRaMacMibSetRequestConfirm( &mibReq );
}

void setDevAddress(uint32_t devadr){
    MibRequestConfirm_t mibReq;
    mibReq.Type = MIB_DEV_ADDR;
    mibReq.Param.DevAddr = devadr;
    LoRaMacMibSetRequestConfirm( &mibReq );
}

void setADR(bool ADROnOff){
    MibRequestConfirm_t mibReq;
    mibReq.Type = MIB_ADR;
    mibReq.Param.AdrEnable=ADROnOff;
    LoRaMacMibSetRequestConfirm( &mibReq );
}

// From LoRaMac
uint32_t LoRaMacGetFCntUp( uint32_t* currentUp );
uint32_t LoRaMacSetFCntUp( uint32_t currentUp );
void ResetFCnts(void);
void setLoRaWanFrameCounter(uint32_t val){
    ResetFCnts();
    LoRaMacSetFCntUp(val);
}

uint32_t getLoRaWanFrameCounter(){
    uint32_t val;
    LoRaMacGetFCntUp(&val);
    return val;
}

uint8_t getNbRep(){
    MibRequestConfirm_t mibReq;
    mibReq.Type = MIB_CHANNELS_NB_TRANS;
    LoRaMacMibGetRequestConfirm( &mibReq );
    return  mibReq.Param.ChannelsNbTrans;
}

uint8_t getCodingRate(){
    MibRequestConfirm_t mibReq;
    mibReq.Type = MIB_CHANNELS_CODING_RATE;
    LoRaMacMibGetRequestConfirm( &mibReq );
    return  mibReq.Param.ChannelsCodingRate;
}

uint8_t getTxPower(){
    MibRequestConfirm_t mibReq;
    mibReq.Type = MIB_CHANNELS_TX_POWER;
    LoRaMacMibGetRequestConfirm( &mibReq );
    return  mibReq.Param.ChannelsTxPower;
}

uint8_t getDataRate(){
    MibRequestConfirm_t mibReq;
    mibReq.Type = MIB_CHANNELS_DATARATE;
    LoRaMacMibGetRequestConfirm( &mibReq );
    return  mibReq.Param.ChannelsDatarate;
}

uint32_t getDevAddress(){
    MibRequestConfirm_t mibReq;
    mibReq.Type = MIB_DEV_ADDR;
    LoRaMacMibGetRequestConfirm( &mibReq );
    return  mibReq.Param.DevAddr;
}

bool getADR(){
    MibRequestConfirm_t mibReq;
    mibReq.Type = MIB_ADR;
    LoRaMacMibGetRequestConfirm( &mibReq );
    return  mibReq.Param.AdrEnable;
}

