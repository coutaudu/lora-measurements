#ifndef LORAMAC_HANDLER_MP_H
#define LORAMAC_HANDLER_MP_H

#include "LoRaMac.h"

/* [IN] nbTrans in [1;15] */
void setNbRep(uint8_t nbTrans);
/* [IN] power in TX_POWER_0=14dBm TX_POWER_1=12dBm TX_POWER_2=10dBm TX_POWER_3=8dBm TX_POWER_4=6dBm TX_POWER_5=4dBm TX_POWER_6=2dBm TX_POWER_7=0dBm */
void setTxPower(uint8_t power);
/* [IN] dataRate in DR_0 SF12 - BW125 DR_1 SF11 - BW125 DR_2 SF10 - BW125 DR_3 SF9 - BW125 DR_4 SF8 - BW125 DR_5 SF7 - BW125 DR_6 SF7 - BW250 */
void setDataRate(uint8_t dataRate);
void setADR(bool ADROnOff);
void setDevAddress(uint32_t devadr);
void setLoRaMacProvisionning();
void setLoRaWanFrameCounter(uint32_t val);
uint32_t getLoRaWanFrameCounter();
uint8_t getNbRep();
uint8_t getTxPower();
uint8_t getDataRate();
bool    getADR();
uint32_t getDevAddress();
void setCodingRate(uint8_t cr);

#endif
