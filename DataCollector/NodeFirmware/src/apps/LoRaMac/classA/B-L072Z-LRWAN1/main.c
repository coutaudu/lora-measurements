#include "main.h"


/**
 * Main application entry point.
 */
int main( void )
{
    BoardInitMcu( );
    printf( "\r\n###### ===== ======================================================== ==== ######\n" );
    printf( "###### ===== Micro-Controler ON                                       ==== ######\n" );
    BoardInitPeriph( );
    printf( "###### ===== Board Peripherals ON                                     ==== ######\n" ); // N.B. no périph i=on the B-L072Z
    setDeviceLoramacEventsHandlers();
    setLoRaMacProvisionning();
    LoRaMacStart( );
    printf( "###### ===== LoRaMAC ON                                               ==== ######\n" );
    printf( "###### ===== ClassA v1.0.RC1                                          ==== ######\n" );
    printf( "###### ===== DevAddr : %08lX                                       ==== ######\n", DevAddr );
    printf( "###### ===== NwkKey :");
    for( int i = 0; i < 16; i++ ){ printf( " %02X", NwkKey[i] );}
    printf(" ==== ######\n");
    printf( "###### ===== Activation Type: ABP                                     ==== ######\n" );
    printf( "###### ===== NwkSKey:" );
    for( int i = 0; i < 16; i++ ){ printf( " %02X", FNwkSIntKey[i] ); }
    printf(" ==== ######\n");
    printf( "###### ===== AppSKey:");
    for( int i = 0; i < 16; i++ ){ printf( " %02X", AppSKey[i] ); }
    printf(" ==== ######\n");
    TimerInit( &TxNextPacketTimer, OnTxNextPacketTimerEvent );
    DeviceState = DEVICE_STATE_SEND;
    printf( "###### ===== ======================================================== ==== ######\r\n" );

    while( 1 )
	{
	    // Process Radio IRQ
	    if( Radio.IrqProcess != NULL ) {
		Radio.IrqProcess( );
	    }
	    // Processes the LoRaMac events
	    LoRaMacProcess( );

	    switch( DeviceState ) {
	    case DEVICE_STATE_SEND:
		ApplicationTxFrame();
		SendFrame();
		DeviceState = DEVICE_STATE_CYCLE;
		break;
	    case DEVICE_STATE_CYCLE:
		DeviceState = DEVICE_STATE_SLEEP;
		break;
	    case DEVICE_STATE_SLEEP:
		{
		    CRITICAL_SECTION_BEGIN( );
		    if( IsMacProcessPending == 1 ){
			// Clear flag and prevent MCU to go into low power modes.
			IsMacProcessPending = 0;
		    }else{
			// The MCU wakes up through events
			BoardLowPowerHandler( );
		    }
		    CRITICAL_SECTION_END( );
		    break;
		}
	    default:
		DeviceState = DEVICE_STATE_SEND;
		break;
	    }
	}
}


/****************************
 * User Application         *
 ****************************/

/*****************
 *** VARIABLES ***
 *****************/
static uint8_t _EXPERIMENT_STATE_ = _XP_INIT_;
static uint8_t _OFFSET_ = 0;
static uint8_t _CONFIG_BASE_ = 0;
static uint8_t _NB_CONFIG_ = 192;
const uint8_t  (* _CONFIG_LUT_)[4];

void setExperimentState(uint8_t* buffer);
static void AskExperimentState();
static void updateExperimentState();

/*****************/
/*** UPLINKS   ***/
/*****************/
static void ApplicationTxFrame( ){
    AppPort = _EXPERIMENT_STATE_;
    if (getLoRaWanFrameCounter()==65536){ setLoRaWanFrameCounter(0); } // Frame counter check MUST be disabed at serveur side (it avoid false negative). So, we CANNOT handle FrmCnt MSB.

    if (_EXPERIMENT_STATE_ == _XP_INIT_){
	printf( "\n\t+-+-+ +-+-+-+-+-+-+-+- +-+-+");
	printf  ( "\n\t+-+-+ REQUEST XP STATE +-+-+ \n");
	AskExperimentState();
	printf( "\t+-+-+ +-+-+-+-+-+-+-+- +-+-+ \r\n");
    } else {

	printf( "\n\t+-+-+ +-+-+-+-+-+-+-+- +-+-+ ");
	printf( "\n\t+-+-+ PREPARE TX FRAME +-+-+ ");

	uint8_t config = (_CONFIG_BASE_ + _OFFSET_) % _NB_CONFIG_;
	uint8_t dr     = 12-_CONFIG_LUT_[config][0]; 
	uint8_t cr     = _CONFIG_LUT_[config][1];
	uint8_t txp    = _CONFIG_LUT_[config][2];
	uint8_t pls    = _CONFIG_LUT_[config][3];
	
	AppDataBuffer[0] = _CONFIG_BASE_;
	AppDataBuffer[1] = _OFFSET_;
	AppPort = _EXPERIMENT_STATE_;
	setDataRate(dr);
	setCodingRate(cr);
	setTxPower(txp);
	AppDataSizeBackup = AppDataSize = pls;

	printf( "\n\t+-+-+ XP         [%d]   +-+-+ ", _EXPERIMENT_STATE_);
	printf( "\n\t+-+-+ CONFIG   [%3d]   +-+-+ ", config);
	printf( "\n\t+-+-+ DATARATE   [%d]   +-+-+ ", dr);
	printf( "\n\t+-+-+ CODERATE   [%d]   +-+-+ ", cr);
	printf( "\n\t+-+-+ TXP        [%d]   +-+-+ ",txp);
	printf( "\n\t+-+-+ PAYLOAD  [%3d]   +-+-+ ",pls);
	printf( "\n\t+-+-+ +-+-+-+-+-+-+-+- +-+-+ \r\n");

	updateExperimentState();
    }   



}
/*****************/
/*** DOWNLINKS ***/
/*****************/
void ApplicationRxFrame(McpsIndication_t *mcpsIndication){ 

    TimerStop( &TxNextPacketTimer );

    uint8_t command = mcpsIndication->Buffer[0];
    printf("##### ----- -------------------- ----- #####\n");
    switch (command) {
    case CMD_STATE_ANSWER:
	printf("##### ----- CMD: [STATE_ANSWER]  ----- #####\n");
	if (_EXPERIMENT_STATE_ == _XP_INIT_){
	    setExperimentState((mcpsIndication->Buffer)+1);
	    updateExperimentState();
	} 
	break;
    case CMD_START:
	printf("##### ----- CMD: [START]  ----- #####\n");
	if (_EXPERIMENT_STATE_ == _XP_INIT_){
	    setLoRaWanFrameCounter(0);
	    
	    _EXPERIMENT_STATE_ = 1;
	    _CONFIG_LUT_ = LUT_XP1; 
	    _NB_CONFIG_        = NB_CONFIG_LUT[_EXPERIMENT_STATE_];
	    _CONFIG_BASE_      = randr(0, _NB_CONFIG_-1);
	    _OFFSET_           = 0;
	}
	break;
    case CMD_RESET:
	BoardResetMcu();
	break;
    default:
	printf("Command RFU\r\n");
	//NULL
	break;
    }
    TxDutyCycleTime = APP_TX_DUTYCYCLE + randr(0,APP_TX_DUTYCYCLE_RND);
    TimerSetValue( &TxNextPacketTimer, TxDutyCycleTime );
    TimerStart( &TxNextPacketTimer );

    printf("##### ----- -------------------- ----- #####\n");
}

static void updateExperimentState(){
    _OFFSET_++;
    if (_OFFSET_==_NB_CONFIG_){
	_EXPERIMENT_STATE_ = (_EXPERIMENT_STATE_ % 2) + 1; // _EXPERIMENT_STATE_ = 0 => INIT TODO parametrer en fonction du nombre d'experiences differentes
	_NB_CONFIG_        = NB_CONFIG_LUT[_EXPERIMENT_STATE_]; // TODO modifier pour méthode élégante: pinteur qu'on met a jour vers la bonne variable.
	_CONFIG_BASE_      = randr(0, _NB_CONFIG_-1);
	_OFFSET_           = 0;
	switch (_EXPERIMENT_STATE_){
	case (_XP1_) :
	    _CONFIG_LUT_ = LUT_XP1; 
	    break;
	case (_XP2_) :
	    _CONFIG_LUT_ = LUT_XP2;
	    break;
	}
    }
   	    
}

static void AskExperimentState(){
    AppPort=LORAWAN_STATE_REQUEST_PORT;
    AppDataSizeBackup = AppDataSize = 1;
    AppDataBuffer[0] = CMD_STATE_ANSWER;
    setTxPower(randr(0,NB_TX_POWER-1));
    //    setDataRate(randr(0,NB_SF-1));
    setDataRate(DR_5);
}


// 2bDR0 2bDR1 2bDR2 2bDR3 2bDR4 2bDR5 4blorawanFrameCounter 1btxPowerBase 1btxPowerOffset
// Total 18 bytes
void setExperimentState(uint8_t* buffer){

    printf(   "\t+-+-+ +-+-+-+-+-+-+-+- +-+-+ \n");
    printf(   "\t+-+-+ STATE XP RESTORE +-+-+ \n");
    _CONFIG_BASE_ = buffer[0];
    _OFFSET_ = buffer[1];
    _EXPERIMENT_STATE_ = buffer[2];
    switch (_EXPERIMENT_STATE_){
    case (_XP1_) :
	_CONFIG_LUT_ = LUT_XP1; 
	break;
    case (_XP2_) :
	_CONFIG_LUT_ = LUT_XP2;
	break;
    }

    uint32_t lrwnfrmCnt = buffer[6] | (buffer[5] << 8) | (buffer[4] << 16) | (buffer[3] << 24);
    setLoRaWanFrameCounter(lrwnfrmCnt); 
    printf(   "\t+-+-+ XP     [%u]       +-+-+ \n", _EXPERIMENT_STATE_);
    printf(   "\t+-+-+ CONFIG [%3u]     +-+-+ \n", _CONFIG_BASE_);
    printf(   "\t+-+-+ OFFSET [%3u]     +-+-+ \n", _OFFSET_);
    printf(   "\t+-+-+ FrmCnt [%6lu] +-+-+ \n", lrwnfrmCnt);
    printf(   "\t+-+-+ +-+-+-+-+-+-+-+- +-+-+ \n");
}



/*!
 * \brief   Prepares the payload of the frame
 *
 * \retval  [0: frame could be send, 1: error]
 */
static bool SendFrame( void )
{
    McpsReq_t mcpsReq;
    LoRaMacTxInfo_t txInfo;

    if( LoRaMacQueryTxPossible( AppDataSize, &txInfo ) != LORAMAC_STATUS_OK )
    	{
    	    printf("NOT LORAMAC_STATUS_OK");
    	    // Send empty frame in order to flush MAC commands
    	    mcpsReq.Type = MCPS_UNCONFIRMED;
    	    mcpsReq.Req.Unconfirmed.fBuffer = NULL;
    	    mcpsReq.Req.Unconfirmed.fBufferSize = 0;
    	    mcpsReq.Req.Unconfirmed.Datarate = LORAWAN_DEFAULT_DATARATE;
    	}
    else
	{
	    mcpsReq.Type = MCPS_UNCONFIRMED;
	    mcpsReq.Req.Unconfirmed.fPort = AppPort;
	    mcpsReq.Req.Unconfirmed.fBuffer = AppDataBuffer;
	    mcpsReq.Req.Unconfirmed.fBufferSize = AppDataSize;
	    mcpsReq.Req.Unconfirmed.Datarate = getDataRate();
	}

    // Update global variable
    AppData.MsgType = LORAMAC_HANDLER_UNCONFIRMED_MSG;
    AppData.Port = mcpsReq.Req.Unconfirmed.fPort;
    AppData.Buffer = mcpsReq.Req.Unconfirmed.fBuffer;
    AppData.BufferSize = mcpsReq.Req.Unconfirmed.fBufferSize;

    LoRaMacStatus_t status;
    status = LoRaMacMcpsRequest( &mcpsReq );
    
    if( status == LORAMAC_STATUS_OK )
	{
	    return false;
	}
    printf(">> Send Frame Failed [%s]\r\n", MacStatusStrings[status]);
    return true;
}

/*!
 * \brief Function executed on TxNextPacket Timeout event
 */
static void OnTxNextPacketTimerEvent( void* context )
{
    MibRequestConfirm_t mibReq;
    LoRaMacStatus_t status;

    TimerStop( &TxNextPacketTimer );

    mibReq.Type = MIB_NETWORK_ACTIVATION;
    status = LoRaMacMibGetRequestConfirm( &mibReq );

    if( status == LORAMAC_STATUS_OK )
	{
	    if( mibReq.Param.NetworkActivation == ACTIVATION_TYPE_NONE )
		{
		    // Network not joined yet. Try to join again
		    // JoinNetwork( );
		}
	    else
		{
		    DeviceState = DEVICE_STATE_SEND;

		}
	}
}


/*!
 * \brief   MCPS-Confirm event function
 *
 * \param   [IN] mcpsConfirm - Pointer to the confirm structure,
 *               containing confirm attributes.
 */
// Data Send uplink
uint8_t uplinkCnt=0;
static void McpsConfirm( McpsConfirm_t *mcpsConfirm )
{
    printf( "             -------------------\n" );
    printf( "###### ===== UPLINK FRAME %6lu ===== ######\n", mcpsConfirm->UpLinkCounter );
    printf( "             -------------------\n" );

    //    Schedule next packet transmission
    TxDutyCycleTime = APP_TX_DUTYCYCLE + randr(0,APP_TX_DUTYCYCLE_RND);
    TimerSetValue( &TxNextPacketTimer, TxDutyCycleTime );
    TimerStart( &TxNextPacketTimer );
}


// Downlink
/*!
 * \brief   MCPS-Indication event function
 *
 * \param   [IN] mcpsIndication - Pointer to the indication structure,
 *               containing indication attributes.
 */
uint8_t nbRcv = 0;
static void McpsIndication( McpsIndication_t *mcpsIndication ) {
    //    GpioWrite(&Led4,nbRcv++);
    printf( "###### ===== =================== ===== ######\n" );
    printf( "###### ===== DOWNLINK  [ %6lu] ===== ######\n", mcpsIndication->DownLinkCounter );    
    printf( "###### ===== Status[%11s] ===== ######\n", EventInfoStatusStrings[mcpsIndication->Status]);
    if( mcpsIndication->Status != LORAMAC_EVENT_INFO_STATUS_OK ){ return;}


    
    const char *slotStrings[] = { "1", "2", "C", "Ping-Slot", "Multicast Ping-Slot" };

    printf( "###### ===== RX WINDOW [%s]       ===== ######\n", slotStrings[mcpsIndication->RxSlot] );
    printf( "###### ===== RX PORT   [%2d]      ===== ######\n", mcpsIndication->Port );
    if( mcpsIndication->BufferSize != 0 ){
	printf( "###### ===== RX DATA             ===== ######\n" );
	printf( "###### ===== ");
	PrintHexBuffer( mcpsIndication->Buffer, mcpsIndication->BufferSize );
	printf("= ######\n" );
    }
    printf( "###### ===== DATA RATE [DR_%d]    ===== ######\n", mcpsIndication->RxDatarate );
    printf( "###### ===== RX RSSI   [%6d]  ===== ######\n", mcpsIndication->Rssi );
    printf( "###### ===== RX SNR    [%6d]  ===== ######\n", mcpsIndication->Snr );
    printf( "###### ===== =================== ===== ######\r\n" );


    if( mcpsIndication->FramePending == true ){ // The server signals that it has pending data to be sent.
	OnTxNextPacketTimerEvent( NULL ); // We schedule an uplink as soon as possible to flush the server.
    }
    
    if ( mcpsIndication->RxData == true ){
	if (mcpsIndication->Port == APPLICATION_DOWNLINK_PORT){ // Coutaudu Downlink Command
	    ApplicationRxFrame(mcpsIndication);
	}
    }

}



/*!
 * \brief   MLME-Confirm event function
 *
 * \param   [IN] mlmeConfirm - Pointer to the confirm structure,
 *               containing confirm attributes.
 */
static void MlmeConfirm ( MlmeConfirm_t *mlmeConfirm )
{
    printf( "MlmeConfirm      : %s\r\n", EventInfoStatusStrings[mlmeConfirm->Status] );
    //printf("McpsIndication: [%s]\r\n", EventInfoStatusStrings[mcpsIndication->Status]);//
}

/*!
 * \brief   MLME-Indication event function
 *
 * \param   [IN] mlmeIndication - Pointer to the indication structure.
 */
static void MlmeIndication( MlmeIndication_t *mlmeIndication )
{
    printf( "MlmeIndication      : %s\r\n", EventInfoStatusStrings[mlmeIndication->Status] );
    if( mlmeIndication->Status != LORAMAC_EVENT_INFO_STATUS_OK )
	{
	    printf( "\r\n###### ===== MLME-Indication ==== ######\r\n" );
	    printf( "STATUS      : %s\r\n", EventInfoStatusStrings[mlmeIndication->Status] );
	    
	    return;
	}
    switch( mlmeIndication->MlmeIndication )
	{
        case MLME_SCHEDULE_UPLINK:
	    {// The MAC signals that we shall provide an uplink as soon as possible
		printf("The MAC signals that we shall provide an uplink as soon as possible\r\n");
		OnTxNextPacketTimerEvent( NULL );
		break;
	    }
        default:
            break;
	}
}

void OnMacProcessNotify( void )
{
    IsMacProcessPending = 1;
}


static LoRaMacStatus_t setDeviceLoramacEventsHandlers(){
    static LoRaMacPrimitives_t macPrimitives;
    static LoRaMacCallback_t macCallbacks;
    LoRaMacStatus_t status;
    macPrimitives.MacMcpsConfirm = McpsConfirm;
    macPrimitives.MacMcpsIndication = McpsIndication;
    macPrimitives.MacMlmeConfirm = MlmeConfirm;
    macPrimitives.MacMlmeIndication = MlmeIndication;
    macCallbacks.GetBatteryLevel = BoardGetBatteryLevel;
    macCallbacks.GetTemperatureLevel = NULL;
    macCallbacks.NvmContextChange = NvmCtxMgmtEvent;
    macCallbacks.MacProcessNotify = OnMacProcessNotify;

    status = LoRaMacInitialization( &macPrimitives, &macCallbacks, ACTIVE_REGION );

    return status;

}


/*!
 * Prints the provided buffer in HEX
 * 
 * \param buffer Buffer to be printed
 * \param size   Buffer size to be printed
 */
void PrintHexBuffer( uint8_t *buffer, uint8_t size )
{
    uint8_t newline = 0;

    for( uint8_t i = 0; i < size; i++ )
	{
	    if( newline != 0 )
		{
		    printf( "\r\n" );
		    newline = 0;
		}

	    printf( "%02X ", buffer[i] );

	    if( ( ( i + 1 ) % 32 ) == 0 )
		{
		    newline = 1;
		}
	}
}

void printLedValue(uint8_t val){
    val = val % 8;
    uint8_t vled1 = (val) & 0x1;
    uint8_t vled3 = (val>>1) & 0x1;
    uint8_t vled4 = (val>>2) & 0x1;

    GpioWrite( &Led1, 0 );
    GpioWrite( &Led3, 0 );
    GpioWrite( &Led4, 0 );
    GpioWrite( &Led1, vled1 );
    GpioWrite( &Led3, vled3 );
    GpioWrite( &Led4, vled4 );
}

void setLoRaMacProvisionning(){
    MibRequestConfirm_t mibReq;
    mibReq.Type = MIB_ABP_LORAWAN_VERSION;
    mibReq.Param.AbpLrWanVersion.Value = ABP_ACTIVATION_LRWAN_VERSION;
    LoRaMacMibSetRequestConfirm( &mibReq );

    mibReq.Type = MIB_APP_KEY;
    mibReq.Param.AppKey = AppKey;
    LoRaMacMibSetRequestConfirm( &mibReq );
    
    mibReq.Type = MIB_NWK_KEY;
    mibReq.Param.NwkKey = NwkKey;
    LoRaMacMibSetRequestConfirm( &mibReq );
    
    mibReq.Type = MIB_NET_ID;
    mibReq.Param.NetID = LORAWAN_NETWORK_ID;
    LoRaMacMibSetRequestConfirm( &mibReq );

    /**/
    mibReq.Type = MIB_DEV_ADDR;
    mibReq.Param.DevAddr = DevAddr;
    LoRaMacMibSetRequestConfirm( &mibReq );
    /**/
    mibReq.Type = MIB_NWK_S_ENC_KEY;
    mibReq.Param.NwkSEncKey = NwkSEncKey;
    LoRaMacMibSetRequestConfirm( &mibReq );

    /**/
    mibReq.Type = MIB_APP_S_KEY;
    mibReq.Param.AppSKey = AppSKey;
    LoRaMacMibSetRequestConfirm( &mibReq );
    
    mibReq.Type = MIB_F_NWK_S_INT_KEY;
    mibReq.Param.FNwkSIntKey = FNwkSIntKey;
    LoRaMacMibSetRequestConfirm( &mibReq );
    
    mibReq.Type = MIB_S_NWK_S_INT_KEY;
    mibReq.Param.SNwkSIntKey = SNwkSIntKey;
    LoRaMacMibSetRequestConfirm( &mibReq );
	
    
    mibReq.Type = MIB_PUBLIC_NETWORK;
    mibReq.Param.EnablePublicNetwork = LORAWAN_PUBLIC_NETWORK;
    LoRaMacMibSetRequestConfirm( &mibReq );
    
    mibReq.Type = MIB_ADR;
    mibReq.Param.AdrEnable = LORAWAN_ADR_ON;
    LoRaMacMibSetRequestConfirm( &mibReq );
    
    LoRaMacTestSetDutyCycleOn( LORAWAN_DUTYCYCLE_ON );

    mibReq.Type = MIB_SYSTEM_MAX_RX_ERROR;
    mibReq.Param.SystemMaxRxError = 20;
    LoRaMacMibSetRequestConfirm( &mibReq );

    mibReq.Type = MIB_NETWORK_ACTIVATION;
    mibReq.Param.NetworkActivation = ACTIVATION_TYPE_ABP;
    LoRaMacMibSetRequestConfirm( &mibReq );


}
