/*!
 * \file      main.c
 *
 * \brief     LoRaMac classA device implementation
 *
 * \copyright Revised BSD License, see section \ref LICENSE.
 *
 * \code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 * \endcode
 *
 * \author    Miguel Luis ( Semtech )
 *
 * \author    Gregory Cristian ( Semtech )
 */

/*! \file classA/B-L072Z-LRWAN1/main.c */
#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include "utilities.h"
#include "board.h"
#include "gpio.h"
#include "LoRaMac.h"
#include "Commissioning.h"
#include "NvmCtxMgmt.h"
#include "config_lut.h"
#include "LoRaMacHandlers.h"

#ifndef ACTIVE_REGION

#warning "No active region defined, LORAMAC_REGION_EU868 will be used as default."

#define ACTIVE_REGION LORAMAC_REGION_EU868

#endif

/*!
 * Defines the application data transmission duty cycle. Value in [ms].
 */
#define APP_TX_DUTYCYCLE                            5000

/*!
 * Defines a random delay for application data transmission duty cycle. Value in [ms].
 */
#define APP_TX_DUTYCYCLE_RND                        250

/*!
 * Default datarate
 */
#define LORAWAN_DEFAULT_DATARATE                    DR_5

/*!
 * LoRaWAN confirmed messages
 */
#define LORAWAN_CONFIRMED_MSG_ON                    false

/*!
 * LoRaWAN Adaptive Data Rate
 *
 * \remark Please note that when ADR is enabled the end-device should be static
 */
#define LORAWAN_ADR_ON                              0


#include "LoRaMacTest.h"

/*!
 * LoRaWAN ETSI duty cycle control enable/disable
 *
 * \remark Please note that ETSI mandates duty cycled transmissions. Use only for test purposes
 */
#define LORAWAN_DUTYCYCLE_ON                        false


/*!
 * LoRaWAN application port
 */
#define LORAWAN_APP_PORT                            2
#define LORAWAN_STATE_REQUEST_PORT                  10
#define APPLICATION_DOWNLINK_PORT LORAWAN_STATE_REQUEST_PORT

//static uint8_t DevEui[] = LORAWAN_DEVICE_EUI;
static uint8_t AppKey[] = LORAWAN_APP_KEY;
static uint8_t NwkKey[] = LORAWAN_NWK_KEY;


static uint8_t FNwkSIntKey[] = LORAWAN_F_NWK_S_INT_KEY;
static uint8_t SNwkSIntKey[] = LORAWAN_S_NWK_S_INT_KEY;
static uint8_t NwkSEncKey[] = LORAWAN_NWK_S_ENC_KEY;
static uint8_t AppSKey[] = LORAWAN_APP_S_KEY;

/*!
 * Device address
 */
static uint32_t DevAddr = LORAWAN_DEVICE_ADDRESS;

/*!
 * Application port
 */
static uint8_t AppPort = LORAWAN_APP_PORT;

/*!
 * User application data size
 */
static uint8_t AppDataSize = 1;
static uint8_t AppDataSizeBackup = 1;

/*!
 * User application data buffer size
 */
#define LORAWAN_APP_DATA_MAX_SIZE                           242

/*!
 * User application data
 */
static uint8_t AppDataBuffer[LORAWAN_APP_DATA_MAX_SIZE];

/*!
 * Indicates if the node is sending confirmed or unconfirmed messages
 */
//static uint8_t IsTxConfirmed = LORAWAN_CONFIRMED_MSG_ON;

/*!
 * Defines the application data transmission duty cycle
 */
static uint32_t TxDutyCycleTime;

/*!
 * Timer to handle the application data transmission duty cycle
 */
static TimerEvent_t TxNextPacketTimer;

/*!
 * Specifies the state of the application LED
 */
//static bool AppLedStateOn = false;

/*!
 * Timer to handle the state of LED1
 */
//static TimerEvent_t Led1Timer;

/*!
 * Timer to handle the state of LED3
 */
//static TimerEvent_t Led3Timer;

/*!
 * Indicates if a new packet can be sent
 */
//legacy static bool NextTx = true;

/*!
 * Indicates if LoRaMacProcess call is pending.
 * 
 * \warning If variable is equal to 0 then the MCU can be set in low power mode
 */
static uint8_t IsMacProcessPending = 0;

/*!
 * Device states
 */
static enum eDeviceState
{
    DEVICE_STATE_RESTORE,
    DEVICE_STATE_START,
    DEVICE_STATE_JOIN,
    DEVICE_STATE_SEND,
    DEVICE_STATE_CYCLE,
    DEVICE_STATE_SLEEP
}DeviceState;


/*!
 *
 */
typedef enum
{
    LORAMAC_HANDLER_UNCONFIRMED_MSG = 0,
    LORAMAC_HANDLER_CONFIRMED_MSG = !LORAMAC_HANDLER_UNCONFIRMED_MSG
}LoRaMacHandlerMsgTypes_t;

/*!
 * Application data structure
 */
typedef struct LoRaMacHandlerAppData_s
{
    LoRaMacHandlerMsgTypes_t MsgType;
    uint8_t Port;
    uint8_t BufferSize;
    uint8_t *Buffer;
}LoRaMacHandlerAppData_t;

LoRaMacHandlerAppData_t AppData =
{
    .MsgType = LORAMAC_HANDLER_UNCONFIRMED_MSG,
    .Buffer = NULL,
    .BufferSize = 0,
    .Port = 0
};

/*!
 * LED GPIO pins objects
 */
extern Gpio_t Led1; // Tx
extern Gpio_t Led3; // Rx
extern Gpio_t Led4; // App

#define NB_TX_POWER 8
#define NB_SF 6


void printLedValue(uint8_t val);

/*! * \brief   Prepares the payload of the frame */
static void ApplicationTxFrame( );
static void ApplicationRxFrame(McpsIndication_t *mcpsIndication);
static void setExperimentState(uint8_t* buffer);

//static void PrepareTxFrameRandomConfig( );

/*! * \brief   Prepares the payload of the frame \retval  [0: frame could be send, 1: error] */
static bool SendFrame( void );

/*! * \brief Function executed on TxNextPacket Timeout event */
static void OnTxNextPacketTimerEvent( void* context );

/* Raise when the frame was emitted */
/*! * \brief   MCPS-Confirm event function
 * \param   [IN] mcpsConfirm - Pointer to the confirm structure,
 *               containing confirm attributes. */
static void McpsConfirm( McpsConfirm_t *mcpsConfirm );

/*! * \brief   MCPS-Indication event function
 * \param   [IN] mcpsIndication - Pointer to the indication structure,
 *               containing indication attributes. */
static void McpsIndication( McpsIndication_t *mcpsIndication );

/*! * \brief   MLME-Confirm event function
 * \param   [IN] mlmeConfirm - Pointer to the confirm structure,
 *               containing confirm attributes. */
static void MlmeConfirm( MlmeConfirm_t *mlmeConfirm );

/*! * \brief   MLME-Indication event function
 *
 * \param   [IN] mlmeIndication - Pointer to the indication structure. */
static void MlmeIndication( MlmeIndication_t *mlmeIndication );

void OnMacProcessNotify( void );

static LoRaMacStatus_t setDeviceLoramacEventsHandlers();


#define CMD_STATE_ANSWER 0
#define CMD_START 1
#define CMD_RESET 2




void PrintHexBuffer( uint8_t *buffer, uint8_t size );

/*!   
 * MAC status strings
 */
const char* MacStatusStrings[] =
    {
	"OK",                            // LORAMAC_STATUS_OK
	"Busy",                          // LORAMAC_STATUS_BUSY
	"Service unknown",               // LORAMAC_STATUS_SERVICE_UNKNOWN
	"Parameter invalid",             // LORAMAC_STATUS_PARAMETER_INVALID
	"Frequency invalid",             // LORAMAC_STATUS_FREQUENCY_INVALID
	"Datarate invalid",              // LORAMAC_STATUS_DATARATE_INVALID
	"Frequency or datarate invalid", // LORAMAC_STATUS_FREQ_AND_DR_INVALID
	"No network joined",             // LORAMAC_STATUS_NO_NETWORK_JOINED
	"Length error",                  // LORAMAC_STATUS_LENGTH_ERROR
	"Region not supported",          // LORAMAC_STATUS_REGION_NOT_SUPPORTED
	"Skipped APP data",              // LORAMAC_STATUS_SKIPPED_APP_DATA
	"Duty-cycle restricted",         // LORAMAC_STATUS_DUTYCYCLE_RESTRICTED
	"No channel found",              // LORAMAC_STATUS_NO_CHANNEL_FOUND
	"No free channel found",         // LORAMAC_STATUS_NO_FREE_CHANNEL_FOUND
	"Busy beacon reserved time",     // LORAMAC_STATUS_BUSY_BEACON_RESERVED_TIME
	"Busy ping-slot window time",    // LORAMAC_STATUS_BUSY_PING_SLOT_WINDOW_TIME
	"Busy uplink collision",         // LORAMAC_STATUS_BUSY_UPLINK_COLLISION
	"Crypto error",                  // LORAMAC_STATUS_CRYPTO_ERROR
	"FCnt handler error",            // LORAMAC_STATUS_FCNT_HANDLER_ERROR
	"MAC command error",             // LORAMAC_STATUS_MAC_COMMAD_ERROR
	"ClassB error",                  // LORAMAC_STATUS_CLASS_B_ERROR
	"Confirm queue error",           // LORAMAC_STATUS_CONFIRM_QUEUE_ERROR
	"Unknown error",                 // LORAMAC_STATUS_ERROR
    };

/*!                                                                                                                  
 * MAC event info status strings.                                                                                    
 */
const char* EventInfoStatusStrings[] =
    {
	"OK",                            // LORAMAC_EVENT_INFO_STATUS_OK
	"Error",                         // LORAMAC_EVENT_INFO_STATUS_ERROR
	"Tx timeout",                    // LORAMAC_EVENT_INFO_STATUS_TX_TIMEOUT
	"Rx 1 timeout",                  // LORAMAC_EVENT_INFO_STATUS_RX1_TIMEOUT
	"Rx 2 timeout",                  // LORAMAC_EVENT_INFO_STATUS_RX2_TIMEOUT
	"Rx1 error",                     // LORAMAC_EVENT_INFO_STATUS_RX1_ERROR
	"Rx2 error",                     // LORAMAC_EVENT_INFO_STATUS_RX2_ERROR
	"Join failed",                   // LORAMAC_EVENT_INFO_STATUS_JOIN_FAIL
	"Downlink repeated",             // LORAMAC_EVENT_INFO_STATUS_DOWNLINK_REPEATED
	"Tx DR payload size error",      // LORAMAC_EVENT_INFO_STATUS_TX_DR_PAYLOAD_SIZE_ERROR
	"Downlink too many frames loss", // LORAMAC_EVENT_INFO_STATUS_DOWNLINK_TOO_MANY_FRAMES_LOSS
	"Address fail",                  // LORAMAC_EVENT_INFO_STATUS_ADDRESS_FAIL
	"MIC fail",                      // LORAMAC_EVENT_INFO_STATUS_MIC_FAIL
	"Multicast fail",                // LORAMAC_EVENT_INFO_STATUS_MULTICAST_FAIL
	"Beacon locked",                 // LORAMAC_EVENT_INFO_STATUS_BEACON_LOCKED
	"Beacon lost",                   // LORAMAC_EVENT_INFO_STATUS_BEACON_LOST
	"Beacon not found"               // LORAMAC_EVENT_INFO_STATUS_BEACON_NOT_FOUND
    };

#endif
