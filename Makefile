# Makefile lora-measurements

COMPRESSED_DATA=Grenoble_France_28_05_2019_to_04_06_2019.json.tar.bz2
RAW_DATA=Grenoble_France_28_05_2019_to_04_06_2019.json
TAR=tar
TAR_OPT=xjf
TEMP-REP=WORKING-REP/
PARSER-REP=XP-Parser
PARSER-Exec=ParserEngine.jar
JAVA=java
JAR=-jar

init:
	mkdir -p $(TEMP-REP) 

$(TEMP-REP):
	make init

extract: $(TEMP-REP)
	tar xjf $(COMPRESSED_DATA) -C $(TEMP-REP)

$(TEMP-REP)$(RAW_DATA):
	make extract

ParserEngine:
	cd $(PARSER-REP); make $(PARSER-Exec); mv $(PARSER-Exec) ../$(TEMP-REP);

$(TEMP-REP)$(PARSER-Exec):
	make ParserEngine

ParseRawData: $(TEMP-REP)$(RAW_DATA) $(TEMP-REP)$(PARSER-Exec)
	cd $(TEMP-REP); $(JAVA) $(JAR) $(PARSER-Exec) --auto; 

mrproper:
	cd $(PARSER-REP); make mrproper;
	rm -rf $(TEMP-REP);	
