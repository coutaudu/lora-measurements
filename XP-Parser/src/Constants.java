import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;

public class Constants {

	/* XPv2 */
	static final int _CONFIG_BASE_INDEX_ = 0;
	static final int _OFFSET_INDEX_ = 1;

	/* XPv1 */
	static final int RAW_DATA_BYTE_CNT_SF_OFFSET = 0;
	static final int RAW_DATA_BYTE_DR = 12;
	static final int RAW_DATA_BYTE_TxP_BASE = 13;
	static final int RAW_DATA_BYTE_TxP_OFFSET = 14;
	
	static final int[] TxPowerLUT = new int[] {14,12,10,8,6,4,2,0,14}; // Rattrape tpo du firmware pour dataXP2part 1 et 2 : randTx tiré entre 0 et NB_TX_POWER et non pas NB_TW_POWER-1
	static final int[] SF_LUT = new int[] {12,11,10,9,8,7,7};
	static final int[] BW_LUT =new int[] {125,125,125,125,125,125,250};
	static final int[] TOA_LUT =new int[] {1646592,905216,411648,226304,123392,66816}; // TODO ne dois pas être utlisé: soit mesure par une GW/NS soit calcul a partir de Payload length,CR, SF...
	static final double NB_FRAME_AVG = 20.0;

	static final int NB_TX_POWER = 8;
	static final int NB_DATARATE = 6;
	
//	static final String defaultInputFile = "./data_XP20042020_Performances_LoRaWAN_COUTAUDU.json"; // TODO read from command line
//	static final String defaultInputFile = "./temp.json"; // TODO read from command line
//	static final String defaultTargetDevice = "1-15rueeliecartan3emeetage";
	static final String defaultInputFile = "./Grenoble_France_28_05_2019_to_04_06_2019.json"; // TODO read from command line
	static final String defaultTargetDevice = "device1"; // TODO read from command line

	static final String defaultOutputRepertory = "./";

	public static int getAppCounter(JSONArray lorawanPayload, int DR) throws ParseException {
		int lsb = Integer.parseInt(lorawanPayload.get((DR*2)+Constants.RAW_DATA_BYTE_CNT_SF_OFFSET).toString());
		int msb = (Integer.parseInt(lorawanPayload.get((DR*2)+Constants.RAW_DATA_BYTE_CNT_SF_OFFSET+1).toString())<<8);
		int app_counter= lsb | msb ;
		return app_counter;
	}
	
	
}
