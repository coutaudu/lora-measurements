import java.nio.file.*;
import java.util.*;

public class ParserEngine {
	
	public static void main(String[] args) {
		ParserToJSON parserToJSON;
//		ParserToCSV parserToCSV;

		String inputFile = Constants.defaultInputFile;
		String outputRepertory = Constants.defaultOutputRepertory;
		String targetDevice = Constants.defaultTargetDevice;
		
		System.out.println("ParserEngine for LoRa Experimental Measurements.");
		System.out.println("Reshape raw experimental data.");
		System.out.println("[1] Filter wanted/unwanted device.");
		System.out.println("[2] Patch received LoRaWAN Counter and compute real LoRaWAN Counter (disable at the NS for the experiment to avoid false negative).");
		System.out.println("[3] Re-sequence LoRaWAN frame. (De-sequencing happen due to latency and jitter between GWs and NS).");
		System.out.println("[4] Write this corrected experimental stream into a buffer file.");
		System.out.println("[5] Multiplex each configuration stream <TxP, SF, GW>. (Warning: this part is specific to the experiment format).");
		System.out.println("[6] Write to CSV files for R later analysis.");
		System.out.println();

		if (!((args.length==1) && args[0].equalsIgnoreCase("--auto"))){
			inputFile = getInput();
			outputRepertory = getOutput();
			targetDevice = getDevice();
		}

		parserToJSON =  new ParserToJSON(inputFile, targetDevice, outputRepertory, true);
		parserToJSON.run();

		for (int i_txP=0; i_txP<Constants.NB_TX_POWER; i_txP++) {
			System.out.println("\t<<"+ i_txP+">>");
			for (int i_GW=0; i_GW<parserToJSON.deviceTargetReceivingGW.size(); i_GW++) {
				String targetGateway = parserToJSON.deviceTargetReceivingGW.get(i_GW);
				System.out.println("\t<<"+ i_txP+">>"+"\t<"+targetGateway+">");
//				ParserToCSV engineToCSV = new ParserToCSVv2(targetDevice, targetGateway, i_txP, outputRepertory, parserToJSON.lastLorawanCounter, parserToJSON.lastDate);
				System.out.println("ParserToCSVv1("+targetDevice+", "+targetGateway+", "+i_txP+", "+outputRepertory+", "+parserToJSON.lastReceivedFrame+");");
				ParserToCSV engineToCSV = new ParserToCSVv1(targetDevice, targetGateway, i_txP, outputRepertory, parserToJSON.lastReceivedFrame);
				engineToCSV.run();
//				new Scanner(System.in).nextLine();	
			}
		}
	}

	private static String getInput() {
		Scanner scanner = new Scanner(System.in);
		String keyboardEntry ="";
		boolean validFile = false;
		String inputFile = Constants.defaultInputFile;
		
		/* Get input file */
		System.out.println("Enter input file. (Leave empty for default :"+inputFile+")");
		do {
			keyboardEntry = scanner.nextLine();
			if (!keyboardEntry.isEmpty()) {	
				inputFile = keyboardEntry;
			}
			try {
				validFile = Files.isReadable(Paths.get(inputFile)) && !Files.isDirectory(Paths.get(inputFile));
				if (!validFile) {System.out.println("["+inputFile+"] is not a valid file. Please enter a valid input file.");}
			} catch (Exception e) {
				System.out.println("Invalid entry - " + e.toString());				
			}
			if (validFile) {
				System.out.println("Confirm " + inputFile+ "? [Y/y]");
				validFile = scanner.nextLine().equalsIgnoreCase("y");
			}
		} while (!validFile);
		scanner.close();		
		
		return inputFile;
	}
	
	private static String getOutput() {
		Scanner scanner = new Scanner(System.in);
		String keyboardEntry ="";
		boolean validFile = false;
		String outputRepertory = Constants.defaultOutputRepertory;

		/* Get ouput directory*/
		System.out.println("Enter output directory. (Leave empty for default :"+outputRepertory+")");
		do {
			keyboardEntry = scanner.nextLine();
			if (!keyboardEntry.isEmpty()) {	
				outputRepertory = keyboardEntry;
			}
			try {
				validFile = Files.isReadable(Paths.get(outputRepertory)) 
						&& Files.isWritable(Paths.get(outputRepertory)) 
						&& Files.isDirectory(Paths.get(outputRepertory));
				if (!validFile) {System.out.println("["+outputRepertory+"] is not a valid repertory. Please enter a valid output repertory.");}
			} catch (Exception e) {
				System.out.println("Invalid entry - " + e.toString());				
			}
			if (validFile) {
				System.out.println("Confirm " + outputRepertory+ "? [Y/y]");
				if ( scanner.nextLine().equalsIgnoreCase("y") ) {
					System.out.println(outputRepertory+"CSV/ and " + outputRepertory+"JSON/ will be erased if they exist. Are you sure ? [Y/y]");
					validFile = scanner.nextLine().equalsIgnoreCase("y");
				}else {
					validFile=false;
				}
			}
		} while (!validFile);
		scanner.close();		
		return outputRepertory;	
	}
	
	private static String getDevice() {
		Scanner scanner = new Scanner(System.in);
		String keyboardEntry ="";
		String targetDevice = Constants.defaultTargetDevice;
		do {
			System.out.println("Enter the ID of the target device. (Leave empty for default: <"+targetDevice+">).");
			keyboardEntry = scanner.nextLine();
			if (!keyboardEntry.isEmpty()) {	
				targetDevice = keyboardEntry;
			}
			System.out.println("Confirm " + targetDevice+ "? [Y/y]");
		}while (!scanner.nextLine().equalsIgnoreCase("y"));
		scanner.close();
		return targetDevice;
	}
	
	
	
	


}
