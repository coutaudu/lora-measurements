import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Vector;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public abstract class ParserToCSV implements Runnable {
	protected DateTimeFormatter myDateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
	static JSONParser jsonParser = new JSONParser();
	protected int lastLorawanCounter;

	// 2b[SF12cnt|SF11cnt|SF10cnt|SF9cnt|SF8cnt|SF7cnt]1b[DR]1b[TxPBase]1b[TxPOffset]
	// 0-11:SFcnt; 12:DR; 13:TxP; 14:nbRep; 15:ADR 
	private String outputRepertory ;
	Path pathFileCSVFull;
	protected String gatewayID;
	protected String deviceID;
	protected List<String> dataLines;
	protected frame currentFrame ;
	protected int previousLoRaWANCounter = 0; // LoRaWAN counter start at 1.
	protected String previousDate;
	private String fileSrc;
	

	protected String lastDate;	
	
	/** For v1 compatibility */
	public ParserToCSV( String device, String gateway, int i_txP, String outputRepertory,  String deviceLastReceivedFrame) {
		try {
			this.gatewayID=gateway;
			this.fileSrc = device+"/"+gateway+"/txP"+i_txP+"/"+device+"_"+gateway+"_txP_"+i_txP;
			this.outputRepertory =  outputRepertory;
			this.currentFrame = new frame();
			this.gatewayID = gateway;
			this.deviceID = device; 
			this.lastDate = parseTime(((JSONObject) ((JSONObject) jsonParser.parse(deviceLastReceivedFrame)).get("metadata")).get("time").toString());
			this.getLastCounter(deviceLastReceivedFrame);

			Path pathFileSrc = Paths.get(outputRepertory+"JSON/"+fileSrc+".json");
			if(Files.exists(pathFileSrc)) {
				this.dataLines = Files.readAllLines(pathFileSrc);

			}else {
				this.dataLines = new Vector<String>();
			}
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}

	
	abstract protected void getLastCounter(String deviceLastReceivedFrame);

	/* ** *** *************** *** ** */
	/* ** ***     RUN         *** ** */
	/* ** *** *************** *** ** */
	JSONObject currentLine; // Placed here for debug-log-trace purpose. TODO
	@Override
	public void run() {
		try {
			this.pathFileCSVFull = Paths.get(outputRepertory+"CSV/data.csv");
			if (!Files.exists(pathFileCSVFull)) {
				Files.write(this.pathFileCSVFull, frame.csvHeader.getBytes(), StandardOpenOption.APPEND, StandardOpenOption.CREATE);
			}
			for (int i=0 ; i<dataLines.size(); i++) {
				currentLine	 = ((JSONObject) jsonParser.parse(dataLines.get(i)));
				if (parseline(currentLine)) {
					frame[] lostFrames = computeErasedFrames();
					writeErasedFrames(lostFrames);
					writeReceivedFrame();
					this.previousLoRaWANCounter=this.currentFrame.getLorawanCounter();
				}
			}
			frame[] lostFrames = computeErasedFrames();
			writeErasedFrames(lostFrames);
		} catch (IOException | ParseException  e) {
			e.printStackTrace();
		} 
	}
	

	/* ** *** *************** *** ** */
	/* ** *** PARSE JSON LINE *** ** */
	/* ** *** *************** *** ** */
	private boolean parseline(JSONObject loraPacket) throws ParseException {
		boolean isDuplicatedFrame;
		currentFrame.setSuccess(true);
		isDuplicatedFrame = getFrameIds(loraPacket);
		JSONArray lorawanPayload = ((JSONArray)((JSONObject)loraPacket.get("payload_raw")).get("data"));
		parsePayload(lorawanPayload); // Traite la payload LoRaWAN adapté à bonne version
		getMetadata((JSONObject) loraPacket.get("metadata"));
		return isDuplicatedFrame;
	}

	private boolean getFrameIds(JSONObject loraPacket) {
		currentFrame.setDevId(loraPacket.get("dev_id").toString());
		currentFrame.setPort(((Long)loraPacket.get("port")).intValue());
		currentFrame.setLorawanCounter(((Long)(loraPacket.get("counter"))).intValue());
		if(currentFrame.getLorawanCounter()<=previousLoRaWANCounter) {
			return false; // Correct bug dublicate udp packets (from cea chamrousse) TODO -> faire dans parserToJSON ?
		}
		return true;
	}
	
	// Parse and comute arameters of the frames. Depends on the experimenta pattern.
	protected abstract void parsePayload(JSONArray lorawanPayload) throws ParseException ;


	private void getMetadata(JSONObject metadata) throws ParseException {
		previousDate = currentFrame.getDate();
		currentFrame.setDate(parseTime(metadata.get("time").toString()));
		currentFrame.setFrequency(metadata.get("frequency").toString());
		checkDataRate(metadata.get("data_rate").toString());
		currentFrame.setTimeOnAir((Long) metadata.get("airtime"));
		currentFrame.setCodingRate(metadata.get("coding_rate").toString());

		// Get GW receiption info
		JSONArray jarray = (JSONArray) metadata.get("gateways");
		JSONObject jsonObjectGatewayInfos = (JSONObject)jsonParser.parse(jarray.get(0).toString()); // NB: 0 because already splitted by GW. 
		currentFrame.setGtwId(jsonObjectGatewayInfos.get("gtw_id").toString());
		currentFrame.setRssi( Double.parseDouble(jsonObjectGatewayInfos.get("rssi").toString()));
		currentFrame.setSnr( Double.parseDouble(jsonObjectGatewayInfos.get("snr").toString()));

	}

	private static String parseTime(String toParseTime) {
		String retval;
		retval = toParseTime.split("T")[0] + "T";
		retval += toParseTime.split("T")[1].split(":")[0] + ":" ;
		retval += toParseTime.split("T")[1].split(":")[1] + ":" ;
		retval += toParseTime.split("T")[1].split(":")[2].split("\\.")[0] ;
		return retval;
	}
	
	private void checkDataRate(String data_rate) {
		String sf = (data_rate.split("BW")[0]).split("SF")[1];
		if (!sf.equalsIgnoreCase(Integer.toString(currentFrame.getSpreadingFactor()))){ 
			System.out.println("Config SF = "+currentFrame.getSpreadingFactor());
			System.out.println("Metadata SF = "+sf);
			System.out.println("Current frame - [sf"+currentFrame.getSpreadingFactor()+
					";cr"+currentFrame.getCodingRate()+
					";txp"+(2*(7-currentFrame.getTxPower()))+"] "
							+ "{snr="+this.currentFrame.getSnr()+"}");
			System.out.println(currentLine.toJSONString());
			JSONArray lorawanPayload = ((JSONArray)((JSONObject)currentLine.get("payload_raw")).get("data"));
			System.out.println(lorawanPayload.toJSONString());
			throw new RuntimeException("SF does no match !");
		}
		String bw = (data_rate.split("BW")[1]);
		if (!bw.equalsIgnoreCase(Integer.toString(currentFrame.getBandwidth()))){ throw new RuntimeException("BW does no match !");}
	}
	/* ** *** ****** *** ** */
	/* ** *** OUTPUT *** ** */
	/* ** *** ****** *** ** */
	
	
	private void writeReceivedFrame() throws IOException {
		/** "app_id, gtw_id, dev_id, hardware_serial, port, lorawan_counter, applicative_counter, time_stamp, sf, bw, tx_power, coding_rate, payload_length, time_on_air, rssi, snr, success\n" */
		String S = currentFrame.toCSVString();
		Files.write(this.pathFileCSVFull, S.getBytes(), StandardOpenOption.APPEND, StandardOpenOption.CREATE);
		File targetDir = new File(this.outputRepertory+"CSV/"+this.deviceID+"/"+this.gatewayID+"/txP"+currentFrame.getTxPower());
		if (!targetDir.exists()) {
			targetDir.mkdirs();
		}
		String targetFile = targetDir+"/"+this.deviceID+"_"+this.gatewayID+"_txP"+currentFrame.getTxPower()+"_DR"+currentFrame.getDataRate()+".csv";
		Files.write(Paths.get(targetFile), S.getBytes(), StandardOpenOption.APPEND, StandardOpenOption.CREATE);
	}

	// Version dependant
	protected void writeErasedFrames(frame[] lostFrames) throws IOException {
		for (int i=0; i<lostFrames.length; i++) {
			String S = lostFrames[i].toCSVString();
			Files.write(this.pathFileCSVFull, S.getBytes(), StandardOpenOption.APPEND, StandardOpenOption.CREATE);
			File targetDir = new File(this.outputRepertory+"CSV/"+this.deviceID+"/"+this.gatewayID+"/txP"+lostFrames[i].getTxPower());
			if (!targetDir.exists()) {
				targetDir.mkdirs();
			}
			String targetFile = targetDir+"/"+this.deviceID+"_"+this.gatewayID+"_txP"+lostFrames[i].getTxPower()+"_DR"+lostFrames[i].getDataRate()+".csv";
			Files.write(Paths.get(targetFile), S.getBytes(), StandardOpenOption.APPEND, StandardOpenOption.CREATE);
		}
	}

	protected abstract frame[] computeErasedFrames() ;

	
	
	protected static final int LORAWAN_HEADERS = 13;
	/** Compute TOA in ms * 10^-3 */
	protected static int lorawanTimeOnAirCalculator(int SF, int payloadSize) {
		return  lorawanTimeOnAirCalculator(SF, 125, 5, payloadSize);
	}

	/**
	 * Compute lorawan frame Time On Air in ms.
	 * @param SF the spreading factor
	 * @param khzBw the bandwidth in kilo hertz
	 * @param payloadSize the payload size in bytes
	 * @return the frame time on air in ms
	 */
	protected static int lorawanTimeOnAirCalculator(int SF, int khzBw, int CRindex,  int payloadSize) {
		int lowDRopt, BW, Tsym, Tpayload, Tpreamble, nSymbolPayload, Tpacket, EH;
		if (SF>=11){
			lowDRopt=1;
		}else{
			lowDRopt=0;
		}
		EH=0; // #0 pour lorawan: explicit header yes    
		BW=khzBw;
		Tsym=(int) ((Math.pow(2, SF)*1000)/BW);
		Tpreamble=(int) ((8+4.25)*Tsym);
		double nominateur = (double)(8*payloadSize-4*SF+28+16-(20*EH));
		double denominateur = (double)(4*(SF-2*lowDRopt));
		nSymbolPayload = 8+(Math.max(((int)Math.ceil(nominateur/denominateur))*(CRindex),0));
		Tpayload = nSymbolPayload*Tsym;
		Tpacket = (Tpreamble+Tpayload) *1000;
		return Tpacket;

	}

	
	
	
	protected class DateEstimator {
		LocalDateTime nextInstant;
		Duration uplinksCadency;

		DateEstimator(int nextLorawanCounter){
			boolean lastErasedFrames;
			int nbLost;
			LocalDateTime previousInstant;
			Duration durationOfErasure;

			if ( dataLines.size()==0) {// Case all frames are lost for this  configuration. We set 1 uplink every 1 seconds. TODO compute from date and counter of first and last frame.
				nextInstant = LocalDateTime.parse(lastDate, myDateFormat);
				uplinksCadency = Duration.ofSeconds(1); 

			} else {
				lastErasedFrames = (currentFrame.getLorawanCounter()==previousLoRaWANCounter) || dataLines.size()==0;

				if(!lastErasedFrames) {
					nextLorawanCounter = currentFrame.getLorawanCounter();
					nbLost = nextLorawanCounter - previousLoRaWANCounter - 1;
					nextInstant = LocalDateTime.parse(currentFrame.getDate(), myDateFormat); 
					if(previousDate != null) { // if more info is available, i.e a first frame was previously received, we narrow our estimation.
						previousInstant  = LocalDateTime.parse(previousDate, myDateFormat); 
						durationOfErasure = Duration.between(previousInstant, nextInstant);
						uplinksCadency = durationOfErasure.dividedBy(nbLost+1);
					}else { // First frame, so we estimate uplink cadency of the entire stream, as it is suppose to be regular over the full experiment, it is quite accurate.
						previousInstant = LocalDateTime.parse(currentFrame.getDate(), myDateFormat); 
						nextInstant = LocalDateTime.parse(lastDate, myDateFormat);
						durationOfErasure = Duration.between(previousInstant, nextInstant);
						uplinksCadency = durationOfErasure.dividedBy(lastLorawanCounter - currentFrame.getLorawanCounter());
						nextInstant = previousInstant;
					}
				} else {
					nextLorawanCounter = lastLorawanCounter + 1;	
					nbLost = nextLorawanCounter - previousLoRaWANCounter - 1;
					previousInstant = LocalDateTime.parse(currentFrame.getDate(), myDateFormat); 
					nextInstant = LocalDateTime.parse(lastDate, myDateFormat);
					durationOfErasure = Duration.between(previousInstant, nextInstant);
					uplinksCadency = durationOfErasure.dividedBy(nbLost+1);
				}
			}
		}
	}


}
