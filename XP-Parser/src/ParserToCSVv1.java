import java.util.Arrays;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;


public class ParserToCSVv1 extends ParserToCSV {

	protected Integer[] previousStateApplicativeCounterBySF;
	protected Integer[] newStateApplicativeCounterBySF;
	protected Integer[] deviceCounterMax ;
	private int[] nbMissed;
	private int totalMissed=0;
	int txPower ;
	
	public ParserToCSVv1(String device, String gateway, int i_txP, String outputRepertory, String deviceLastReceivedFrame) {
		super(device, gateway, i_txP, outputRepertory, deviceLastReceivedFrame);
		this.txPower = 14-(2*i_txP); 
		this.previousStateApplicativeCounterBySF = new Integer [] {0,0,0,0,0,0};
		this.newStateApplicativeCounterBySF = new Integer [] {0,0,0,0,0,0};
		this.nbMissed = new int[] {0,0,0,0,0,0};
		this.totalMissed=0;
	}
	
	@Override
	protected void getLastCounter(String deviceLastReceivedFrame) {
		JSONObject parsedLoraPacket;
		this.deviceCounterMax = new Integer [] {0,0,0,0,0,0};
		try {
			parsedLoraPacket = ((JSONObject) jsonParser.parse(deviceLastReceivedFrame));
			this.lastLorawanCounter = (((Long)parsedLoraPacket.get("counter")).intValue());
			JSONArray lorawanPayload = ((JSONArray)((JSONObject)parsedLoraPacket.get("payload_raw")).get("data"));
			for (int datarate=0; datarate<Constants.NB_DATARATE; datarate++) {
				deviceCounterMax[datarate] = Constants.getAppCounter(lorawanPayload, datarate);
			}
			System.out.println(Arrays.toString(deviceCounterMax));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void parsePayload(JSONArray lorawanPayload) throws ParseException {
		currentFrame.setPayloadLength(lorawanPayload.size());
		currentFrame.setDataRate(((Long)lorawanPayload.get(Constants.RAW_DATA_BYTE_DR)).intValue());
		currentFrame.setSpreadingFactor(getSF(currentFrame.getDataRate()));
		currentFrame.setBandwidth(125);
		currentFrame.setAppCounter(Constants.getAppCounter(lorawanPayload, currentFrame.getDataRate()));
		currentFrame.setTxPower(this.txPower);
		for (int datarate=0; datarate<Constants.NB_DATARATE; datarate++){
			newStateApplicativeCounterBySF[datarate] = Constants.getAppCounter(lorawanPayload, datarate);
		}		
	}
	
	
	private static int getSF(int dR2) {
		int SF = Constants.SF_LUT[(dR2)];
		return SF;
	}	
		
	
	protected frame[] computeErasedFrames(int nextLorawanCounter) {
		return computeErasedFrames();
	}
		
	protected frame[] computeErasedFrames() {
		boolean lastErasedFrame;
		int nextLorawanCounter;

		lastErasedFrame = (this.currentFrame.getLorawanCounter()==this.previousLoRaWANCounter);
		if (lastErasedFrame) {
			this.newStateApplicativeCounterBySF =  this.deviceCounterMax;
			this.currentFrame.setDataRate(-1);
			nextLorawanCounter = this.lastLorawanCounter + 1;	
		}else {
			nextLorawanCounter = this.currentFrame.getLorawanCounter();
		}
		DateEstimator DE = new DateEstimator(nextLorawanCounter);

		for (int datarate=0; datarate<Constants.NB_DATARATE; datarate++) {
			computeDataRateLostFrames(datarate);
		}
	
		frame[] retval = new frame[totalMissed];
		
		for (int datarate=0; datarate<Constants.NB_DATARATE;datarate++) {
			for (int j=nbMissed[datarate];j>0;j--) {
				int missedAppCounter;
				if(datarate==currentFrame.getDataRate()) {	missedAppCounter = ((newStateApplicativeCounterBySF[datarate]) - (j));
				}else {				missedAppCounter = ((newStateApplicativeCounterBySF[datarate]) - (j-1));}
				frame lostFrame ;
				lostFrame = new frame();
				lostFrame.setGtwId(currentFrame.getGtwId());
				lostFrame.setDevId(currentFrame.getDevId());
				lostFrame.setPort(currentFrame.getPort());
				lostFrame.setLorawanCounter(currentFrame.getLorawanCounter()-totalMissed);
				lostFrame.setAppCounter(missedAppCounter);
				String erasedDate = (DE.nextInstant.minus(DE.uplinksCadency.multipliedBy(totalMissed))).format(myDateFormat);
						//(instantCurrentFrame.minus(uplinksFrequency.multipliedBy(totalMissed))).format(myDateFormat);
				lostFrame.setDate(erasedDate);
				lostFrame.setDataRate(datarate);
				lostFrame.setSpreadingFactor(Constants.SF_LUT[datarate]);
				lostFrame.setBandwidth(125);
				lostFrame.setTxPower(this.txPower);
				lostFrame.setCodingRate("4/5");
				lostFrame.setPayloadLength(15);
				lostFrame.setFrequency("NA");
				lostFrame.setRssi(-1000);
				lostFrame.setSnr(-1000);
				lostFrame.setSuccess(false);
				long toa =	ParserToCSV.lorawanTimeOnAirCalculator(lostFrame.getSpreadingFactor(), lostFrame.getBandwidth(), 5, lostFrame.getPayloadLength()+LORAWAN_HEADERS);
				lostFrame.setTimeOnAir(toa); 
				retval[retval.length-totalMissed] = lostFrame;	
				totalMissed--;
			}
			nbMissed[datarate]=0;
		}
		return retval;
	}

	private void computeDataRateLostFrames(int datarate) {
		nbMissed[datarate] = newStateApplicativeCounterBySF[datarate] - previousStateApplicativeCounterBySF[datarate];
		totalMissed += nbMissed[datarate];
		previousStateApplicativeCounterBySF[datarate] = newStateApplicativeCounterBySF[datarate];
		// if datarate is the current receive data rate its normal that "previous-1=new"
		if(datarate==currentFrame.getDataRate()) {
			totalMissed --;
			nbMissed[datarate] --;
		}
	}	


}
