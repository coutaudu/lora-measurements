import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class ParserToCSVv2 extends ParserToCSV {
	static boolean logNoticeXPChanged; // debug log purpose only.

	int TOTAL_NB_CONFIGURATIONS;
	Configuration currentConfig = new Configuration();
	Configuration previousConfig = null;


	public ParserToCSVv2(String device, String gateway, int i_txP, String outputRepertory, String deviceLastReceivedFrame) {
		super(device, gateway, i_txP, outputRepertory, deviceLastReceivedFrame);
		TOTAL_NB_CONFIGURATIONS = 0;
		for(int i=1; i<Config_LUT.NB_CONFIG_LUT.length; i++) {
			TOTAL_NB_CONFIGURATIONS += Config_LUT.NB_CONFIG_LUT[i];
		}
		previousConfig = new Configuration();
		previousConfig.xp=1;
		previousConfig.base=0;
		previousConfig.offset=-1;
		previousConfig.index=0;
		previousConfig.configureLUT(1);
	}

	@Override
	protected void getLastCounter(String deviceLastReceivedFrame) {
		JSONObject parsedLorawanPacket;
		try {
			parsedLorawanPacket = ((JSONObject) jsonParser.parse(deviceLastReceivedFrame));
			this.lastLorawanCounter = (((Long)parsedLorawanPacket.get("counter")).intValue());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void parsePayload(JSONArray lorawanPayload) throws ParseException {
		int currentPort = currentFrame.getPort();
		currentConfig.configureLUT(currentPort);
		currentConfig.base = ((Long)lorawanPayload.get(Constants._CONFIG_BASE_INDEX_)).intValue();
		currentConfig.offset = ((Long)lorawanPayload.get(Constants._OFFSET_INDEX_)).intValue();
		currentConfig.index = (currentConfig.base + currentConfig.offset) % currentConfig.LUT_size;
		int appCnt = ParserToCSVv2.computeAppCnt(currentFrame.getLorawanCounter(),TOTAL_NB_CONFIGURATIONS) ;
		setFrameParameters(this.currentFrame, currentConfig, appCnt);
	}
	
	

	@Override
	protected frame[] computeErasedFrames() {
		boolean lastErasedFrame;
		int nextLorawanCounter;
		int nbLost;
		Configuration erasedConfiguration ;

		logNoticeXPChanged=false;
		lastErasedFrame = (this.currentFrame.getLorawanCounter()==this.previousLoRaWANCounter);

		if(!lastErasedFrame) {
			nextLorawanCounter = this.currentFrame.getLorawanCounter();
		} else {
			nextLorawanCounter = this.lastLorawanCounter + 1;	
		}
		nbLost = nextLorawanCounter - this.previousLoRaWANCounter - 1;
		DateEstimator DE = new DateEstimator(nextLorawanCounter);
		frame[] retval = new frame[nbLost];

		for(int i_lost=0; i_lost<nbLost; i_lost++) {
			int remainLost=(nbLost-i_lost);
			int erasedLoRaWANCnt = nextLorawanCounter - remainLost;
			int appCnt = ParserToCSVv2.computeAppCnt(erasedLoRaWANCnt,TOTAL_NB_CONFIGURATIONS) ;
			
			erasedConfiguration = previousConfig;
			erasedConfiguration.next(remainLost, nextLorawanCounter);

			retval[i_lost] = new frame();
			String erasedDate = (DE.nextInstant.minus(DE.uplinksCadency.multipliedBy(remainLost))).format(myDateFormat);
			retval[i_lost].setDate(erasedDate);
			setFrameParameters(retval[i_lost], erasedConfiguration, appCnt);
			setErasedFrameParameters(retval[i_lost], erasedConfiguration.xp, erasedDate, erasedLoRaWANCnt);
			System.out.println("["+erasedLoRaWANCnt+"]L\t= "+erasedConfiguration +"{"+appCnt+"}" + 
					" - frame[sf"+retval[i_lost].getSpreadingFactor() +
					";cr"+retval[i_lost].getCodingRate() +
					";txp"+(2*(7-retval[i_lost].getTxPower()))+"dBm] " +
					"{snr = NA.  "
					+ "; toa = "+retval[i_lost].getTimeOnAir()+"} - "+
					retval[i_lost].getDate());// +" counter["++"]"+ currentLine.toJSONString());

		}
		
		if (currentConfig.offset==0) {
			System.out.println("------------------------------------");
			logNoticeXPChanged=true;
		}
		if(nextLorawanCounter==this.currentFrame.getLorawanCounter()) {
			System.out.println("["+this.currentFrame.getLorawanCounter()+"]R\t= "+currentConfig + "{"+this.currentFrame.getAppCounter()+"}" + 
					" - frame[sf"+currentFrame.getSpreadingFactor()+
					";cr"+currentFrame.getCodingRate()+
					";txp"+(2*(7-currentFrame.getTxPower()))+"dBm] "
					+ "{snr="+this.currentFrame.getSnr()+
					"; toa = "+this.currentFrame.getTimeOnAir() +"} - "+
					currentFrame.getDate()) ;// +" counter["++"]"+ currentLine.toJSONString());
		}

		if (logNoticeXPChanged) { // DEQBUG purpose only.
			logNoticeXPChanged=false;
		}
		previousConfig.copy(currentConfig);
		return retval;
	}

	
	private static int computeAppCnt(int LoRaWANCounter, int totalNbConfig) {
		int appCnt = (LoRaWANCounter-1)/totalNbConfig ; // -1 because lorwan frmade counter start at 0 and not 0.
		return appCnt;
	}

	private void setFrameParameters(frame f, Configuration c, int appCounter) {
		setFrameParameters(f, c.parameters_LUT, c.index, appCounter);
	}
		
	private void setFrameParameters(frame f, int[][] LUT_XP, int config,  int appCounter) {
		f.setDataRate(12-LUT_XP[config][0]);
		f.setSpreadingFactor(LUT_XP[config][0]);
		f.setCodingRate("4/"+(4+LUT_XP[config][1]));
		f.setTxPower(LUT_XP[config][2]);
		f.setPayloadLength( LUT_XP[config][3]);
		f.setBandwidth(125); 
		f.setAppCounter(appCounter);
		long toa =	ParserToCSV.lorawanTimeOnAirCalculator(f.getSpreadingFactor(), f.getBandwidth(), (4+LUT_XP[config][1]), f.getPayloadLength()+LORAWAN_HEADERS);
		f.setTimeOnAir(toa); 
	}
	
	private void setErasedFrameParameters(frame f, int port, String date, int erasedLoRaWANCnt) {
		f.setLorawanCounter(erasedLoRaWANCnt); //
		f.setPort(port); //
		f.setGtwId(this.gatewayID); // 
		f.setDevId(this.deviceID); //
		f.setDate(date);

	}
	

	private class Configuration {
		int xp;
		int base;
		int offset ;
		int index;
		int[][] parameters_LUT ;
		int LUT_size;
		
		
		void copy(Configuration c) {
			this.xp = c.xp;
			this.base = c.base;
			this.offset = c.offset;
			this.index = c.index;
			this.parameters_LUT = c.parameters_LUT;
			this.LUT_size = c.LUT_size;
		}

		public void next(int remainLost, int nextLorawanCounter) {
			this.offset++;
			if(this.offset == this.LUT_size) { // Next XP
				System.out.println("------------------------------------");
				logNoticeXPChanged = true;
				int nextXpState = (previousConfig.xp%(Config_LUT.NB_CONFIG_LUT.length-1))+1; // 1->2 2->1
				this.configureLUT(nextXpState);
				this.offset = 0;
				this.base = 0;
			}
			if(currentConfig.offset==remainLost && nextLorawanCounter==currentFrame.getLorawanCounter()) { // Case we do know the next XP configuration base.
				this.base = currentConfig.base;
			}
			this.index = (this.base + this.offset) % this.LUT_size;
		}

		void configureLUT(int xpState){
			this.xp=xpState;

			switch (xp) {
			case 2:
				this.parameters_LUT = Config_LUT.LUT_XP2;
				break;
			case 1:
				this.parameters_LUT = Config_LUT.LUT_XP1;
				break;
			case 0:
			default:
				throw new RuntimeException("[Invalid XP state received: "+xp+".]");
			}
			this.LUT_size = Config_LUT.NB_CONFIG_LUT[xp];
		}
		
		public String toString() {
			String S =  "XP["+xp+"]["+base+"->"+offset+" = "+index+"]["
					+ this.parameters_LUT[index][0]+";"
					+ this.parameters_LUT[index][1]+";"
					+ this.parameters_LUT[index][2]+";"
					+ this.parameters_LUT[index][3]+"]";
			return S;
			}
		
	}

}
