import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Vector;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ParserToJSON implements Runnable  {

	private String inputFile ;
	private String targetDevice ;
	private String outputRepertory ;
	public Vector<String> deviceTargetReceivingGW;
	private JSONParser parser ;
	private boolean correction;
	
	public String lastReceivedFrame;
//	public int lastLorawanCounter;
//	public String lastDate;
//	Integer[] deviceCounterMax ;

	public ParserToJSON(String inputFile, String targetDevice, String outputRepertory,	boolean correction) {
		super();
		this.inputFile = inputFile;
		this.targetDevice = targetDevice;
		this.outputRepertory = outputRepertory;
		deviceTargetReceivingGW =  new Vector<String>();
		parser = new JSONParser();
		this.correction = correction;
		ParserToJSON.prepareDirectory(outputRepertory+"CSV/");
		ParserToJSON.prepareDirectory(outputRepertory+"JSON/");
	}

	@Override
	public void run() {
		List<String> data;
		try {
			System.out.println("Read Data");
			data = readRawData(inputFile);
			System.out.println("Filter");
			filterDevice(data);
			System.out.println("Correct Offset");
			correctLorawanCounterMSB(data);
			System.out.println("Sort");
			reorder(data);
			System.out.println("Write");
			writeCorrectedData(data,inputFile);
			System.out.println("Multiplex");
			multiplexData(data);
			lastReceivedFrame = data.get(data.size()-1);
		} catch (IOException | ParseException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		
	}
	
	// TODO Implement mecanism to detect if src file untouched since last execution.
	static List<String> readRawData(String fileTarget) {
		List<String> dataLines = null;
		try {
			dataLines = Files.readAllLines(Paths.get(fileTarget));
		} catch (IOException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		return dataLines;
	}


	private void filterDevice(List<String> data) throws IOException, ParseException {
		for (int i=0; i<data.size();i++) {
			String currentDevice = ((JSONObject)(this.parser.parse(data.get(i)))).get("dev_id").toString();
			if (!targetDevice .equalsIgnoreCase(currentDevice)) {
				data.remove(i);
				correction=true;
			}
		}
	}
	
	/** Read lorawan trace in format json and check if lorawan counter is incrementing 
	 * @throws IOException 
	 * @throws ParseException */
	@SuppressWarnings("unchecked")
	private void correctLorawanCounterMSB(List<String> data) throws IOException, ParseException {
		int previousCounter=0;
		int offset=0;
		for (int i=0; i<data.size();i++) {
			JSONObject jsonLine = (JSONObject)(this.parser.parse(data.get(i)));
			int writtenCounter = ((Long)(jsonLine).get("counter")).intValue();
			int counter = writtenCounter + offset;
			if (previousCounter-counter>2000) { // NB. We consider that data desequencing cannot exceed 2000. It is extremely likely to be true.
				correction=true;
				counter += 65536;
				offset += 65536;
 			}
			if(offset!=0) { 
				jsonLine.put("counter", counter);
				data.set(i, jsonLine.toJSONString());
			}

			previousCounter=counter;
		}
//		this.lastLorawanCounter = ((Long)((JSONObject)(this.parser.parse(data.get(data.size()-1)))).get("counter")).intValue();
//		this.lastDate = ((JSONObject)((JSONObject)this.parser.parse(data.get(data.size()-1))).get("metadata")).get("time").toString();
	}


	/** Read lorawan trace in format json and check if lorawan counter is incrementing 
	 * @throws IOException 
	 * @throws ParseException */
	private static void reorder(List<String> data) throws IOException, ParseException {
		lorawanJsonlineComparator c = new lorawanJsonlineComparator();
		data.sort(c);
	}

	/** If needed, rewrite the src file with the corrected data. */
	private  void writeCorrectedData(List<String> data, String fileTarget) throws IOException {
		if (correction) {
			Path file = Paths.get(fileTarget);
			Files.deleteIfExists(file);
			for (int i=0; i<data.size(); i++) {
				Files.write(file, (data.get(i)+"\n").getBytes(), StandardOpenOption.APPEND,  StandardOpenOption.CREATE);
			}
		}
	}

	/** Write a single file for each TxP and GW of the trace. */
	private void multiplexData(List<String> dataLines) throws IOException, ParseException {
		for (int i=0; i<dataLines.size(); i++ ) {
			String line = renameKnownGW(dataLines.get(i));
			JSONObject jsonTTNLine = (JSONObject)(parser.parse(line));
			parseDataJsonLine(jsonTTNLine);
		}	
	}


	
	
	@SuppressWarnings( "unchecked" )
	 void parseDataJsonLine(JSONObject jsonTTNLine) throws ParseException, IOException {
		JSONArray jsonPayload	 = (JSONArray)((JSONObject)jsonTTNLine.get("payload_raw")).get("data");
		JSONObject jsonMetadata = (JSONObject) jsonTTNLine.get("metadata");
		String devID = (String)jsonTTNLine.get("dev_id");
		int port = ((Long)jsonTTNLine.get("port")).intValue();
		Long txP = computeCurrentTXP(devID, port, jsonPayload );
		JSONArray jarrayGateways = (JSONArray) jsonMetadata.get("gateways");		

		// For each GW that receive this packet:
		for (int i_gw=0; i_gw<jarrayGateways.size();i_gw++) {
			JSONObject jsonObjectGatewayInfos = (JSONObject)this.parser.parse(jarrayGateways.get(i_gw).toString());
			String GW = jsonObjectGatewayInfos.get("gtw_id").toString();
			// If first reception by this GW:
			if (!deviceTargetReceivingGW.contains(GW)) { 
				deviceTargetReceivingGW.add(GW);
			}
			// Prepare JSON to write
			JSONObject jsonSingleGWMetadata = jsonMetadata;
			JSONArray jarraySingleGateway = new JSONArray();
			jarraySingleGateway.add(jsonObjectGatewayInfos);
			jsonSingleGWMetadata.put("gateways", jarraySingleGateway);
			jsonTTNLine.put("metadata", jsonSingleGWMetadata);
			if ( !(new File(outputRepertory+"JSON/"+devID).exists() ) ) {
				
			}
			File targetDir = new File(outputRepertory+"JSON/"+devID+"/"+GW+"/txP"+txP+"/");
			if (!targetDir.exists()) {
				targetDir.mkdirs();
			}
			Path outputFilePath =  Paths.get(targetDir.getAbsolutePath()+"/"+devID+"_"+GW+"_txP_"+txP+".json"); // TODO multiplex avec port également ??? Ou plutot CR et payloadLength
			Files.write(outputFilePath, (jsonTTNLine.toString()+"\n").getBytes(), StandardOpenOption.APPEND,  StandardOpenOption.CREATE);
		}
	}


	
	private static Long computeCurrentTXP(String devID, int port, JSONArray jsonPayload) {
		Long txP;
		int nbConfig;
		switch (devID) {
		case "device1" :
			switch	(port) {
			case 2 :
				Long txPowerBase = (Long) jsonPayload.get(Constants.RAW_DATA_BYTE_TxP_BASE);
				Long txPowerOffset = (Long) jsonPayload.get(Constants.RAW_DATA_BYTE_TxP_OFFSET);
				txP = (txPowerBase + txPowerOffset) % Constants.NB_TX_POWER;
				break;
			default:
				throw new RuntimeException("["+devID+"]Unknown port.");
			}
			break;
		case "1-15rueeliecartan3emeetage" :
			Long configBase   = (Long) jsonPayload.get(0);
			Long configOffset = (Long) jsonPayload.get(1);
			switch	(port) {
			case 1 :
			case 2 :
				nbConfig = Config_LUT.NB_CONFIG_LUT[port];
				break;
			default:
				throw new RuntimeException("["+devID+"]Unknown port.");
			}
			Long config = (configBase + configOffset) % nbConfig;
			txP = config % Constants.NB_TX_POWER;
			break;
		default:
			throw new RuntimeException("["+devID+"]Unknown device.");
		}	
		return txP;
	}



	@SuppressWarnings("unchecked")
	private static String renameKnownGW(String stringJSONLine) {
		//Olivier Gimenez
		stringJSONLine = stringJSONLine.replaceAll("eui-904d4afffeff4d8e", "OG-SagemCom-d8e");
		stringJSONLine = stringJSONLine.replaceAll("eui-904d4afffeff4dd5", "OG-SagemCom-dd5");
		stringJSONLine = stringJSONLine.replaceAll("eui-904d4afffeff4dd3", "OG-SagemCom-dd3");
		stringJSONLine = stringJSONLine.replaceAll("eui-904d4afffeff4c03", "OG-SagemCom-c03");
		stringJSONLine = stringJSONLine.replaceAll("eui-904d4afffeff4c91", "OG-SagemCom-c91");

		stringJSONLine = stringJSONLine.replaceAll("eui-7276fffffe019882", "OG-Kerlink-882");
		stringJSONLine = stringJSONLine.replaceAll("eui-7276fffffe0198ac", "OG-Kerlink-8ac");
		stringJSONLine = stringJSONLine.replaceAll("eui-7276fffffe01988f", "OG-Kerlink-88f");
		stringJSONLine = stringJSONLine.replaceAll("eui-7276fffffe019843", "OG-Kerlink-843");
		stringJSONLine = stringJSONLine.replaceAll("eui-7276fffffe019881", "OG-Kerlink-881");
		stringJSONLine = stringJSONLine.replaceAll("eui-7276fffffe01914a", "OG-Kerlink-14a");

		//COUTAUDU
		stringJSONLine = stringJSONLine.replaceAll("eui-3535303229007200", "CU-PicoGW-200");

		//IMAG
		stringJSONLine = stringJSONLine.replaceAll("eui-0000024b08040056", "IMAG-Kerlink");
		stringJSONLine = stringJSONLine.replaceAll("eui-3535303229006f00", "IMAG-PicoGW-f00");

		//Semtech
		stringJSONLine = stringJSONLine.replaceAll("eui-b827ebfffe7776c0", "Semtech-1301");

		return stringJSONLine;
	}

	private static void prepareDirectory(String directory) {
		File csvFile = new File(directory);
		if (csvFile.exists()) {
			String files[] = csvFile.list();
			for(int i=0; i<files.length	; i++) {
				File todel = new File(directory+files[i]);
				 deleteDir(todel);
			}
		} else {
			csvFile.mkdirs();
		}
		

	}

	private static boolean deleteDir(File file) {
	    File[] contents = file.listFiles();
	    if (contents != null) {
	        for (File f : contents) {
	            deleteDir(f);
	        }
	    }
	    return file.delete();
	}



}
