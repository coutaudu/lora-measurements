
public class frame {
	
	/** "app_id, gtw_id, dev_id, hardware_serial, port, lorawan_counter, applicative_counter, time_stamp, sf, bw, tx_power, coding_rate, payload_length, time_on_air, rssi, snr, success\n" */
	private String gtwId;
	private String devId;
	private int port;
	private int lorawanCounter;
	private int appCounter;
	private String date;

	private int dataRate;
	private int spreadingFactor;
	private int bandwidth;
	private int txPower;
	private String codingRate;
	private int payloadLength;
	private Long timeOnAir;
	private String frequency;
	private double rssi;
	private double snr;
	private boolean success;
	
	
	/** "gtw_id, dev_id, hardware_serial, port, lorawan_counter, applicative_counter, time_stamp, sf, bw, tx_power, coding_rate, payload_length, time_on_air, rssi, snr, success\n" */
	public static String csvHeader = "gtw_id, dev_id, port, lorawan_counter, applicative_counter, time_stamp,"
			+ " sf, bw, tx_power, coding_rate, payload_length,"
			+ " time_on_air, rssi, snr, success\n";

	public String toCSVString() {
		String retval;
		retval = this.getGtwId() + "," + this.getDevId() + "," +
				+ this.getPort() + "," + this.getLorawanCounter() + "," + this.getAppCounter() + "," + this.getDate() + ",";
		retval +=  this.getSpreadingFactor() +"," + this.getBandwidth() + "," + this.getTxPower() + "," + this.getCodingRate() + "," + this.getPayloadLength() + ",";
		if(this.isSuccess()) {
			retval += this.getTimeOnAir() + ","	+ this.getRssi() + "," + this.getSnr() + "," + "1"  ; 
		} else {
			retval += this.getTimeOnAir() + ","	+ "NA" + "," + "NA" + "," + "0" ; 
		}
		retval += "\n"	;
		return retval;
	}

	public frame() {
		super();	
	}
		
	public frame(String appId, String gtwId, String devId, String hardwareSerial, int port, int lorawanCounter,
			int appCounter, String date, int dataRate, int spreadingFactor, int bandwidth, int txPower,
			String codingRate, int payloadLength, Long timeOnAir, String frequency, double rssi, double snr,
			boolean success) {
		super();
		this.gtwId = gtwId;
		this.devId = devId;
		this.port = port;
		this.lorawanCounter = lorawanCounter;
		this.appCounter = appCounter;
		this.date = date;
		this.dataRate = dataRate;
		this.spreadingFactor = spreadingFactor;
		this.bandwidth = bandwidth;
		this.txPower = txPower;
		this.codingRate = codingRate;
		this.payloadLength = payloadLength;
		this.timeOnAir = timeOnAir;
		this.frequency = frequency;
		this.rssi = rssi;
		this.snr = snr;
		this.success = success;
	}

	public String getGtwId() {
		return gtwId;
	}
	public void setGtwId(String gtwId) {
		this.gtwId = gtwId;
	}
	public String getDevId() {
		return devId;
	}
	public void setDevId(String devId) {
		this.devId = devId;
	}
	
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public int getLorawanCounter() {
		return lorawanCounter;
	}
	public void setLorawanCounter(int lorawanCounter) {
		this.lorawanCounter = lorawanCounter;
	}
	public int getAppCounter() {
		return appCounter;
	}
	public void setAppCounter(int appCounter) {
		this.appCounter = appCounter;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getDataRate() {
		return dataRate;
	}
	public void setDataRate(int dataRate) {
		this.dataRate = dataRate;
	}
	public int getSpreadingFactor() {
		return spreadingFactor;
	}
	public void setSpreadingFactor(int spreadingFactor) {
		this.spreadingFactor = spreadingFactor;
	}
	public int getBandwidth() {
		return bandwidth;
	}
	public void setBandwidth(int bandwidth) {
		this.bandwidth = bandwidth;
	}
	public int getTxPower() {
		return txPower;
	}
	public void setTxPower(int txPower) {
		this.txPower = txPower;
	}
	public String getCodingRate() {
		return codingRate;
	}
	public void setCodingRate(String codingRate) {
		this.codingRate = codingRate;
	}
	public int getPayloadLength() {
		return payloadLength;
	}
	public void setPayloadLength(int payloadLength) {
		this.payloadLength = payloadLength;
	}
	public Long getTimeOnAir() {
		return timeOnAir;
	}
	public void setTimeOnAir(Long timeOnAir) {
		this.timeOnAir = timeOnAir;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public double getRssi() {
		return rssi;
	}
	public void setRssi(double rssi) {
		this.rssi = rssi;
	}
	public double getSnr() {
		return snr;
	}
	public void setSnr(double snr) {
		this.snr = snr;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	
	@Override
	public String toString() {
		return "frame [gtwId=" + gtwId + ", devId=" + devId 
				+ ", port=" + port + ", lorawanCounter=" + lorawanCounter + ", appCounter=" + appCounter + ", date="
				+ date + ", dataRate=" + dataRate + ", spreadingFactor=" + spreadingFactor + ", bandwidth=" + bandwidth
				+ ", txPower=" + txPower + ", codingRate=" + codingRate + ", payloadLength=" + payloadLength
				+ ", timeOnAir=" + timeOnAir + ", frequency=" + frequency + ", rssi=" + rssi + ", snr=" + snr
				+ ", success=" + success + "]";
	}
	
	
}
