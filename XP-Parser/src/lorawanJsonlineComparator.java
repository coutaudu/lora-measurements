

import java.util.Comparator;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;



public class lorawanJsonlineComparator implements Comparator<String> {

	@Override
	public int compare(String o1, String o2) {
		JSONObject jsonObj;
		JSONParser parser = (new JSONParser());
		try {
			jsonObj = (JSONObject)(parser.parse(o1));
			int counterO1 =  Integer.parseInt(jsonObj.get("counter").toString());
			jsonObj = (JSONObject)(parser.parse(o2));
			int counterO2 =  Integer.parseInt(jsonObj.get("counter").toString());
			return counterO1-counterO2;
		} catch (ParseException e) {
			e.printStackTrace();
			System.exit(1);
		}
		return 0;
	}
	
}