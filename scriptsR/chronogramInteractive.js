var myPlot = document.getElementsByClassName('plotly')[0];
var TBE=(1<1);


function updateHistograms() {
//    alert('UpdateHistogram.');
    if(TBE){
	var xaxisMin=myPlot.layout.xaxis3.range[0].split('.')[0];
	var xaxisMax=myPlot.layout.xaxis3.range[1].split('.')[0];
	var i;
	var len;
	var isHisto;

	var dateAxisTitle='[ '+xaxisMin+' ; '+xaxisMax+' ]';
	myPlot.layout.annotations[0].text=dateAxisTitle;

	for (i = 0, len = myPlot.data.length; i < len; i++) {
            isHisto=myPlot.data[i].name.startsWith('[HISTO]');
            if (isHisto){
//		alert('Modify histogram filter');
		myPlot.data[i].transforms[0].value=xaxisMin;
		myPlot.data[i].transforms[1].value=xaxisMax;
            }
	}
	var update = { opacity: 1 };
	Plotly.restyle(myPlot, update);

	TBE=(1<1);
    }
}

myPlot.on('plotly_relayout', function(){
    TBE=(1==1);
    setTimeout(updateHistograms, 1000);
});



myPlot.on('plotly_click', function(){
    //alert('plotly click');   
    // var xaxisMin=myPlot.layout.xaxis3.range[0].split('.')[0];
    // alert('xaxisMin='+xaxisMin);   
    // alert('filterL='+myPlot.data[0].transforms[0].value);
    // var xaxisMax=myPlot.layout.xaxis3.range[1].split('.')[0];
    // alert('xaxisMax='+xaxisMax);
    // alert('filterH='+myPlot.data[0].transforms[1].value);
});


//myPlot.on('plotly_restyle', function(){ alert('plotly restyle.');});

