#############
## COUTAUD ##
## SEMTECH ##
## LIG     ##
## 2019    ##
#############


#############################
## Declaration de packages ##
#############################

suppressMessages(library(methods))
suppressMessages(library(ggplot2))
suppressMessages(library(dplyr))
suppressMessages(library(scales))

pathBase="../WORKING-REP/"

lineSize=1
pointSize=0.9
legendTextSize=16


myTheme = theme(
    legend.position="right",
    legend.box="vertical",
    legend.text=element_text(size=legendTextSize),
    legend.title=element_text(size=legendTextSize),
    axis.text=element_text(size=legendTextSize),
    axis.text.x=element_text(angle=70, hjust=1),
    axis.title = element_text(size = legendTextSize),
    strip.text = element_text(size=legendTextSize),
    legend.key = element_rect(colour = "transparent", fill = "transparent"),
    legend.background = element_rect(colour = "transparent", fill = "transparent"))



computeMainPlot <- function (dataArg, yTargetLabel, zTargetLabel){
    cat(yTargetLabel);
    cat("  ");
    
    if (yTargetLabel=="RSSI"){
        dataArg <-  filter(dataArg, RSSI!=0); ##TODO remove only for RSSI plottting ?
        yTarget=dataArg$RSSI;
        localCoordCart=c(-50,-140);
    }
    if (yTargetLabel=="NOISE"){ yTarget=dataArg$noise; localCoordCart=c(-50,-140);}
    if (yTargetLabel=="SNR"){ yTarget=dataArg$SNR; localCoordCart=c(-25,20);}
    ##    if (yTargetLabel=="SNRMax"){ yTarget=dataArg$SNR20MAX; localCoordCart=c(-20,20);}
    ##    if (yTargetLabel=="PDR"){ yTarget=1-dataArg$PER20; localCoordCart=c(0,1);}
    ##    if (yTargetLabel=="RSSIMax"){ yTarget=dataArg$RSSI20max; localCoordCart=c(-50,-140);}    


        if (zTargetLabel=="GW") {zTargetValues=dataArg$GW}
    if (zTargetLabel=="SF") {zTargetValues=dataArg$SF}


    localPlot = ggplot(data=dataArg,
                       aes(x=TIMESTAMP, y=yTarget,
                           color=factor(zTargetValues), shape=factor(GW), fill=factor(zTargetValues))) +
        labs(color=zTargetLabel, shape="GW", fill=zTargetLabel) +
        scale_shape_manual(values=seq(0,16))    +
        coord_cartesian(ylim=localCoordCart)    +
        geom_point(size=pointSize)    +
        theme_bw()    +
        myTheme +
        xlab("Time") + ylab(yTargetLabel);
    
    return (localPlot);
}


plotWL <- function (dataArg, filterSF, filterGW, zTarget, DEVICE, filterTime){

############## READ AND PREPARE DATA ###########################################
    data <- dataArg;
    if (filterSF!="ALL"){ data <-  filter(data, SF==filterSF)  }
    if (filterGW!="ALL"){ data <-  filter(data, GW==filterGW) }
    if (filterGW=="ALL"){ data <-  filter(data, GW!="GW1") }

    data$TIMESTAMP <- as.POSIXct(strptime(data$TIMESTAMP,  format="%Y-%m-%dT%H:%M:%S"));
    warnings()
#    myBreaks = function(x){ seq.Date(from=min(x), to=max(x), by="3 hours")}
    
    if ( filterTime == "ONE_DAY" ){
        data <- filter(data, TIMESTAMP>strptime("2019-05-29T00:00:00", format="%Y-%m-%dT%H:%M:%S")
                       & TIMESTAMP<strptime("2019-05-30T00:00:00",  format="%Y-%m-%dT%H:%M:%S"));
    }
    if ( filterTime == "ONE_HOUR_NIGHT"){
        data <- filter(data, TIMESTAMP>strptime("2019-05-29T00:00:00",  format="%Y-%m-%dT%H:%M:%S")
                       & TIMESTAMP<strptime("2019-05-29T01:01:00",  format="%Y-%m-%dT%H:%M:%S"));
    }
    if ( filterTime == "ONE_HOUR_DAY"){
        data <- filter(data, TIMESTAMP>strptime("2019-05-29T12:00:00",  format="%Y-%m-%dT%H:%M:%S")
                       & TIMESTAMP<strptime("2019-05-29T13:00:00",  format="%Y-%m-%dT%H:%M:%S"));
    }
    



############## PLOT ###########################################

    dir.create(paste(pathBase,"Plots/",sep=""));
    path=pathBase;
    path=paste(path,"Plots/",DEVICE,"/",sep="", collapse=NULL)
    dir.create(path)
    path=paste(path,filterGW,"/",sep="", collapse=NULL)
    dir.create(path)
    path=paste(path,filterSF,"/",sep="", collapse=NULL)
    dir.create(path)
    path=paste(path,filterTime,"/",sep="", collapse=NULL)
    dir.create(path)

    filenamePart1 = paste(path,filterTime,"_",zTarget, "_",sep="", collapse=NULL)
    filenamePart2 = paste(".pdf", sep="", collapse=NULL)

    data$noise = data$RSSI - data$SNR


    yTargetLabels=c("SNR", "RSSI", "NOISE");
    for (yTargetLabel in yTargetLabels){
        plot = computeMainPlot(data, yTargetLabel, zTarget)
        plot <- plot + annotate("text", min(data$TIMESTAMP)+1, Inf, label=paste("Dev[",DEVICE,"] SF[",filterSF,"] GW[",filterGW,"] VarY[",yTargetLabel,"]",sep="", collapse=NULL), hjust=0, vjust=2 , color="azure3");
        filename = paste(filenamePart1,yTargetLabel, filenamePart2, sep="", collapse=NULL)
        print(filename)
        ggsave(filename,width=50, height=7,limitsize=FALSE)
    }
    
}



theData <- read.csv(paste(pathBase,"restrictedData.csv",sep=""));

theData <- filter(theData, SUCCESS==1)


theData <-  filter(theData, GW=="GW1" | GW=="GW2" | GW=="GW3" | GW=="GW4" | GW=="GW5" | GW=="GW6" | GW=="GW7" | GW=="GW8" );


devList=c("device1");
SFlist=c("ALL","12","11","10","9","8","7");
GWlist=c("ALL","GW1","GW2","GW3","GW4","GW5","GW6","GW7","GW8");
    
for(DEV in devList ){
    cat(DEV);
    cat("\n++++++++++\n");
    for (GW in GWlist ){
        cat("\n**********\n");
        for (SF in SFlist ){
            cat(paste("[",DEV,"][",GW,"][SF",SF,"]", sep=""));
            cat("ONE_HOUR_DAY\n")
            plotWL(theData, SF, GW, "SF", DEV, "ONE_HOUR_DAY")        
            cat("\nALL_DATA\n")
            plotWL(theData, SF, GW, "SF", DEV, "ALL_DATA")        
            cat("\nONE_DAY\n")
            plotWL(theData, SF, GW, "SF", DEV, "ONE_DAY")
            cat("\nONE_HOUR_NIGHT\n")
            plotWL(theData, SF, GW, "SF", DEV, "ONE_HOUR_NIGHT")
        }

    }
}

